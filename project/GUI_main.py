from wfdb import rdheader, rdsamp
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPalette
from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtWidgets import QInputDialog, QTableWidget
from pyqtgraph import PlotWidget
from visualization_classes import ECGPlot, ECGPlotWidget
import queue
import numpy as np
import math
from datetime import timedelta
import datetime, time

class Ui_MainWindow():

    def __init__(self, q_to_main):  
        self.q_to_main = q_to_main
        self.fs = 0
        self.r_peaks = []
        self.no_of_samples = 0
        self.analyzing = 0
        self.dots = "."
        self.plot1 = 0
        self.plot2 = 0
        self.plot3 = 0
        self.plot4 = 0
        self.plotdfa1 = 0
        self.plotdfa2 = 0
        self.plotdfa3 = 0
        self.plothrv21 = 0
        self.plothrv22 = 0
        self.plotheart1 = 0
        self.plotheart2 = 0
        self.tbl = QtWidgets.QTableWidget(0, 5)

    def readsignal_plot(self):
        self.fs = 0
        self.r_peaks = []
        self.no_of_samples = 0
        self.q_to_main.put({
            "command": "load_path",
            "data": {
                "path": self.POLEDOSCIEZKI.text()
                }
            })

    def dodajzakres_plot(self):
        
        self.off = int(self.ODLINE.text())
        self.n_of_s = int(self.DOLINE.text()) - self.off

        if ((self.off+self.n_of_s <= self.no_of_samples) and self.n_of_s > 1 and self.off >= 0 and int(self.DOLINE.text()) > 0 and self.n_of_s <= 100000 and self.ODLINE.text()!="" and self.DOLINE.text()!=""):
            self.q_to_main.put({
                "command": "request_data",
                "data": {
                    "offset": self.off,
                    "num_of_samples": self.n_of_s
                    }
                })

        else:
            self.POLEDOSCIEZKI.setText("wrong range")

    def start_analyze(self):
        if (self.fs != 0):
            self.analyzing = 1
            self.q_to_main.put({
                "command": "start_rap",
                "data": {
                    "filter_type": self.comboBox.currentIndex(),
                    "r_peaks_type": self.comboBoxTompkins.currentIndex()
                    }
                })

        else:
            self.POLEDOSCIEZKI.setText("Can't analyze. No file loaded")
        return

    def tables(self, hrv1, hrvdfa, heartclass):
        self.HRV_DFA_table.setItem(0, 0, QtWidgets.QTableWidgetItem(str(hrvdfa[4][0])))
        self.HRV_DFA_table.setItem(1, 0, QtWidgets.QTableWidgetItem(str(hrvdfa[4][1])))
        self.HRV_1_table.setItem(0, 0, QtWidgets.QTableWidgetItem(str(hrv1["MeanRR"])[:5]))
        self.HRV_1_table.setItem(1, 0, QtWidgets.QTableWidgetItem(str(hrv1["SDNN"])[:5]))
        self.HRV_1_table.setItem(2, 0, QtWidgets.QTableWidgetItem(str(hrv1["RMSSD"])[:5]))
        self.HRV_1_table.setItem(3, 0, QtWidgets.QTableWidgetItem(str(hrv1["NN50"])[:5]))
        self.HRV_1_table.setItem(4, 0, QtWidgets.QTableWidgetItem(str(hrv1["pNN50"])[:5]))
        self.HRV_1_table.setItem(5, 0, QtWidgets.QTableWidgetItem(str(hrv1["SDANN"])[:5]))
        self.HRV_1_table.setItem(6, 0, QtWidgets.QTableWidgetItem(str(hrv1["SDNNindex"])[:5]))
        self.HRV_1_table.setItem(7, 0, QtWidgets.QTableWidgetItem(str(hrv1["SDSD"])[:5]))
        self.HRV_1_table.setItem(8, 0, QtWidgets.QTableWidgetItem(str(hrv1["total_power"])[:5]))
        self.HRV_1_table.setItem(9, 0, QtWidgets.QTableWidgetItem(str(hrv1["ulf"])[:5]))
        self.HRV_1_table.setItem(10, 0, QtWidgets.QTableWidgetItem(str(hrv1["vlf"])[:5]))
        self.HRV_1_table.setItem(11, 0, QtWidgets.QTableWidgetItem(str(hrv1["lf"])[:5]))
        self.HRV_1_table.setItem(12, 0, QtWidgets.QTableWidgetItem(str(hrv1["hf"])[:5]))
        self.HRV_1_table.setItem(13, 0, QtWidgets.QTableWidgetItem(str(hrv1["lf_hf_ratio"])[:5]))

        sup = 0 
        ven = 0
        un = 0

        for hc in heartclass:
            sup = sup + hc.get_labels_report()["Supraventricular beat"]
            ven = ven + hc.get_labels_report()["Ventricular beat"]
            un = un + hc.get_labels_report()["Unclassified beat/Artifact"]

        self.Heart_Class_table.setItem(0, 0, QtWidgets.QTableWidgetItem(str(sup)))
        self.Heart_Class_table.setItem(1, 0, QtWidgets.QTableWidgetItem(str(ven)))
        self.Heart_Class_table.setItem(2, 0, QtWidgets.QTableWidgetItem(str(un)))

    def Peaks_plot(self):
        if self.radioButton.isChecked():
            self.Wizualizacja.plot_widget.plot_R_peaks(self.r_peaks)
        else:
            self.Wizualizacja.plot_widget.plot_R_peaks([])

    def HRV2_plot(self, hrv2):

        if(self.plothrv21):
            self.hrv2_graphic.removeItem(self.plothrv21)

        if(self.plothrv22):
            self.hrv2_graphic.removeItem(self.plothrv22)

        self.plothrv21 = self.hrv2_graphic.plot(hrv2["rr"][:-1], hrv2["rr"][1:], name="RR", pen = (0,0,0), symbol = 'o', symbolSize = 6)
        self.plothrv22 = self.hrv2_graphic.plot(hrv2["ell_rot"][0,:], hrv2["ell_rot"][1,:], name="RR", pen = (255,0,0))

    def HRV1_plot(self, periodogram):
        X1 = periodogram['X_ULF']
        Y1 = periodogram['Y_ULF']
        
        X2 = periodogram['X_VLF'] 
        Y2 = periodogram['Y_VLF']
        
        X3 = periodogram['X_LF'] 
        Y3 = periodogram['Y_LF'] 
        
        X4 = periodogram['X_HF'] 
        Y4 = periodogram['Y_HF'] 

        if(self.plot1):
            self.hrv1_graphic.removeItem(self.plot1)

        if(self.plot2):
            self.hrv1_graphic.removeItem(self.plot2)

        if(self.plot3):
            self.hrv1_graphic.removeItem(self.plot3)

        if(self.plot4):
            self.hrv1_graphic.removeItem(self.plot4)

        if math.isnan(X1[0]) and math.isnan(X2[0]):
            self.plot1 = self.hrv1_graphic.plot(X3, Y3, name="HRV1", pen = (0,255,0))
            self.plot2 = self.hrv1_graphic.plot(X4, Y4, name="HRV1", pen = (0,0,255))

        elif math.isnan(X1[0]):
            self.plot1 = self.hrv1_graphic.plot(X2, Y2, name="HRV1", pen = (255,255,0))
            self.plot2 = self.hrv1_graphic.plot(X3, Y3, name="HRV1", pen = (0,255,0))
            self.plot3 = self.hrv1_graphic.plot(X4, Y4, name="HRV1", pen = (0,0,255))

        else:
            self.plot1 = self.hrv1_graphic.plot(X1, Y1, name="HRV1", pen = (0,255,0))
            self.plot2 = self.hrv1_graphic.plot(X2, Y2, name="HRV1", pen = (255,255,0))
            self.plot3 = self.hrv1_graphic.plot(X3, Y3, name="HRV1", pen = (0,255,0))
            self.plot4 = self.hrv1_graphic.plot(X4, Y4, name="HRV1", pen = (0,0,255))
            
    def HRVDFA_plot(self, dfa):

        if(self.plotdfa1):
            self.hrvdfa_graphic.removeItem(self.plotdfa1)

        if(self.plotdfa2):
            self.hrvdfa_graphic.removeItem(self.plotdfa2)

        if(self.plotdfa3):
            self.hrvdfa_graphic.removeItem(self.plotdfa3)
        
        self.plotdfa1 = self.hrvdfa_graphic.plot(dfa[0], np.log10(dfa[1]), name="HRV DFA", pen = (0,0,0), symbol = 'o', symbolSize = 10)
        self.plotdfa2 = self.hrvdfa_graphic.plot(dfa[0][0:13], dfa[2], name="HRV DFA", pen = (255,0,0))
        self.plotdfa3 =self.hrvdfa_graphic.plot(dfa[0][13:62], dfa[3], name="HRV DFA", pen = (255,255,0))
        
    def HEART_plot(self, heart_class):

        if(self.plotheart1):
            self.heartclass_graphic.removeItem(self.plotheart1)

        if(self.plotheart2):
            self.heartclass_graphic.removeItem(self.plotheart2)

        x = []
        y = []
        z = []

        for ind, result in enumerate(heart_class):
            if(ind < result.get_number_of_clusters_1()):
                for val in result.get_axis_for_index(1,"X",ind):
                    x.append(val)
                for val in result.get_axis_for_index(1,"Y",ind):
                    y.append(val)
                for val in result.get_axis_for_index(1,"Z",ind):
                    z.append(val)
        
        self.plotheart1 = self.heartclass_graphic.plot(x,z, name="Supraventricular beats clustering", pen = (0,0,0), symbol = '+', symbolSize = 15)
       
        x = []
        y = []
        z = []

        for ind, result in enumerate(heart_class):
            if(ind < result.get_number_of_clusters_2()):
                for val in result.get_axis_for_index(2,"X",ind):
                    x.append(val)
                for val in result.get_axis_for_index(2,"Y",ind):
                    y.append(val)
                for val in result.get_axis_for_index(2,"Z",ind):
                    z.append(val)

        self.plotheart1 = self.heartclass_graphic2.plot(x,z, name="Ventricular beats clustering", pen = (0,0,0), symbol = '+', symbolSize = 15)

    def st_table(self, st_segment):

        for row in np.zeros(self.tbl.rowCount()):
            self.tbl.removeRow(row)
        all_data = []
        for i in range(len(st_segment["offsets"])):
            all_data.append([timedelta(seconds=st_segment["ST_starts"][i]/self.fs), st_segment["offsets"][i], st_segment["slopes"][i], st_segment["episodes"][i], st_segment["shape_episodes"][i]])

        for row in all_data:
            inx = all_data.index(row)
            self.tbl.insertRow(inx)
            self.tbl.setItem(inx, 0, QtWidgets.QTableWidgetItem(str(row[0])))
            self.tbl.setItem(inx, 1, QtWidgets.QTableWidgetItem(str(row[1])))
            self.tbl.setItem(inx, 2, QtWidgets.QTableWidgetItem(str(row[2])))
            self.tbl.setItem(inx, 3, QtWidgets.QTableWidgetItem(str(row[3])))
            self.tbl.setItem(inx, 4, QtWidgets.QTableWidgetItem(str(row[4])))

    def setupUi(self, MainWindow):

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(490, 415)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget_7 = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget_7.setGeometry(QtCore.QRect(10, 10, 771, 800))
        self.verticalLayoutWidget_7.setObjectName("verticalLayoutWidget_7")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_7)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.dadmprojektLABEL = QtWidgets.QLabel(self.verticalLayoutWidget_7)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(165, 194, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(165, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(165, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        self.dadmprojektLABEL.setPalette(palette)
        font = QtGui.QFont()
        font.setFamily("Arial Nova")
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        self.dadmprojektLABEL.setFont(font)
        self.dadmprojektLABEL.setObjectName("dadmprojektLABEL")
        self.verticalLayout.addWidget(self.dadmprojektLABEL)
        self.tabWidget = QtWidgets.QTabWidget(self.verticalLayoutWidget_7)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(55, 155, 255)) # Color of general GUI
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush) # Color of table area
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush) # Color of GUI frame
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 207))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush) # Unknown
        brush = QtGui.QBrush(QtGui.QColor(253, 82, 35))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Highlight, brush) # Color of general GUI un-clicked
        brush = QtGui.QBrush(QtGui.QColor(55, 155, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush) # Unknown
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush) # Color of GUI frame un-clicked
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 207))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush) # Unknown
        brush = QtGui.QBrush(QtGui.QColor(253, 82, 35))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Highlight, brush) # Unknown
        brush = QtGui.QBrush(QtGui.QColor(55, 150, 111))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush) # Unknown
        brush = QtGui.QBrush(QtGui.QColor(203, 236, 207))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush) # Unknown
        brush = QtGui.QBrush(QtGui.QColor(203, 236, 207))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush) # Unknown
        brush = QtGui.QBrush(QtGui.QColor(0, 120, 215))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Highlight, brush)
        self.tabWidget.setPalette(palette)
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.tab)


        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 181, 341))

        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")


        self.ECGlayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.ECGlayout.setContentsMargins(0, 0, 0, 0)
        self.ECGlayout.setObjectName("ECGlayout")

        # PATH ORAZ OKNA DO WPISYWANIA PATH 17.01
        self.PATH = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.PATH.setObjectName("PATH")
        self.ECGlayout.addWidget(self.PATH)
        self.POLEDOSCIEZKI = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.POLEDOSCIEZKI.setObjectName("POLEDOSCIEZKI")
        self.ECGlayout.addWidget(self.POLEDOSCIEZKI)


        self.ButtonReadsignal = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.ButtonReadsignal.setObjectName("ButtonReadsignal")

        self.ECGlayout.addWidget(self.ButtonReadsignal)

        # dodanie zakresu 17.01
        self.ODLABEL = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.ODLABEL.setObjectName("ODLABEL")
        self.ECGlayout.addWidget(self.ODLABEL)
        self.ODLINE = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.ODLINE.setObjectName("ODLINE")
        self.ECGlayout.addWidget(self.ODLINE)
        self.DOLABEL = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.DOLABEL.setObjectName("DOLABEL")
        self.ECGlayout.addWidget(self.DOLABEL)
        self.DOLINE = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.DOLINE.setObjectName("DOLINE")
        self.ECGlayout.addWidget(self.DOLINE)
        self.button_dodajzakres = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.button_dodajzakres.setObjectName("button_dodajzakres")
        self.ECGlayout.addWidget(self.button_dodajzakres)

        self.label_2 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.ECGlayout.addWidget(self.label_2)
        self.comboBox = QtWidgets.QComboBox(self.verticalLayoutWidget)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.ECGlayout.addWidget(self.comboBox)
        self.label_3 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_3.setObjectName("label_3")
        self.ECGlayout.addWidget(self.label_3)
        self.comboBoxTompkins = QtWidgets.QComboBox(self.verticalLayoutWidget)
        self.comboBoxTompkins.setObjectName("comboBoxTompkins")
        self.comboBoxTompkins.addItem("")
        self.comboBoxTompkins.addItem("")
        self.ECGlayout.addWidget(self.comboBoxTompkins)
        self.buttonECGbaseline = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.buttonECGbaseline.setObjectName("buttonECGbaseline")
        self.ECGlayout.addWidget(self.buttonECGbaseline)
       # self.buttonRpeaks = QtWidgets.QPushButton(self.verticalLayoutWidget)
       # self.buttonRpeaks.setObjectName("buttonRpeaks")
       # self.ECGlayout.addWidget(self.buttonRpeaks)
        self.radioButton = QtWidgets.QRadioButton(self.verticalLayoutWidget)
        self.radioButton.setObjectName("radioButton")
        self.ECGlayout.addWidget(self.radioButton)


        self.verticalLayoutWidget_2 = QtWidgets.QWidget(self.tab)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(200, 10, 550, 450))
        self.verticalLayoutWidget_2.setObjectName("verticalLayoutWidget_2")
        self.ECGlayoutvis = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_2)
        self.ECGlayoutvis.setContentsMargins(0, 0, 0, 0)
        self.ECGlayoutvis.setObjectName("ECGlayoutvis")
        self.label_4 = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        self.label_4.setObjectName("label_4")


        self.ECGlayoutvis.addWidget(self.label_4)
        self.Wizualizacja = ECGPlotWidget()


        self.Wizualizacja.setObjectName("Wizualizacja")
        self.ECGlayoutvis.addWidget(self.Wizualizacja)

        self.tabWidget.addTab(self.tab, "")

        #DRUGIE OKNO WAVESY
        self.tabWaves = QtWidgets.QWidget()
        self.tabWaves.setObjectName("tabWaves")
        self.layoutWidget = QtWidgets.QWidget(self.tabWaves)
        self.layoutWidget.setGeometry(QtCore.QRect(20, 30, 191, 221))
        self.layoutWidget.setObjectName("layoutWidget")
        #self.wavelayout = QtWidgets.QVBoxLayout(self.layoutWidget)
       # self.wavelayout.setContentsMargins(0, 0, 0, 0)
      #  self.wavelayout.setObjectName("wavelayout")
       # self.pushButton_5 = QtWidgets.QPushButton(self.layoutWidget)
        #self.pushButton_5.setObjectName("pushButton_5")
        #self.wavelayout.addWidget(self.pushButton_5)
       # self.buttonQRS = QtWidgets.QPushButton(self.layoutWidget)
       # self.buttonQRS.setObjectName("buttonQRS")
       # self.wavelayout.addWidget(self.buttonQRS)
        self.verticalLayoutWidget_Waves = QtWidgets.QWidget(self.tabWaves)
        self.verticalLayoutWidget_Waves.setGeometry(QtCore.QRect(160, 60, 491, 440))
        self.verticalLayoutWidget_Waves.setObjectName("verticalLayoutWidget_Waves")
        self.wavelayoutvis = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_Waves)
        self.wavelayoutvis.setContentsMargins(0, 0, 0, 0)
        self.wavelayoutvis.setObjectName("wavelayoutvis")
        self.labelQRS = QtWidgets.QLabel(self.verticalLayoutWidget_Waves)
        self.labelQRS.setObjectName("labelQRS")
        self.wavelayoutvis.addWidget(self.labelQRS)
        self.wave_graphic = ECGPlotWidget()
        #self.wave_graphic = PlotWidget(self.verticalLayoutWidget_Waves)
        self.wave_graphic.setObjectName("wave_graphic")
        self.wavelayoutvis.addWidget(self.wave_graphic)
        self.tabWidget.addTab(self.tabWaves, "")



        #TRZECIE OKNO SEGMENTACJA
        self.tabSegment = QtWidgets.QWidget()
        self.tabSegment.setObjectName("tabSegment")
        self.SEGMENTlayoutWidget = QtWidgets.QWidget(self.tabSegment)
        self.SEGMENTlayoutWidget.setGeometry(QtCore.QRect(160, 60, 491, 440))
        self.SEGMENTlayoutWidget.setObjectName("layoutWidgetSEGMENT")
        self.Segmentlayoutvis = QtWidgets.QVBoxLayout(self.SEGMENTlayoutWidget)
        self.Segmentlayoutvis.setContentsMargins(0, 0, 0, 0)
        self.Segmentlayoutvis.setObjectName("ECGlayoutvis")
        #self.stsegment_graphic = QtWidgets.QGraphicsView(self.tabSegment)
        #self.stsegment_graphic.setGeometry(QtCore.QRect(30, 20, 701, 431))
       # self.stsegment_graphic.setObjectName("stsegment_graphic")
        self.Segmentacja = ECGPlotWidget()
        self.Segmentacja.setObjectName("Segmetacja")
        self.Segmentlayoutvis.addWidget(self.Segmentacja)



       # self.buttonSegmentRun = QtWidgets.QPushButton(self.tabSegment)
      #  self.buttonSegmentRun.setGeometry(QtCore.QRect(10, 60, 41, 23))
      #  self.buttonSegmentRun.setObjectName("buttonSegmentRun")
        self.tabWidget.addTab(self.tabSegment, "")

        self.tabHrv = QtWidgets.QWidget()
        self.tabHrv.setObjectName("tabHrv")
        self.hrv_widget = QtWidgets.QTabWidget(self.tabHrv)
        self.hrv_widget.setGeometry(QtCore.QRect(60, 20, 700, 450))
        self.hrv_widget.setObjectName("hrv_widget")
        self.tabHrvDfa = QtWidgets.QWidget()
        self.tabHrvDfa.setObjectName("tabHrvDfa")

        self.hrvdfa_graphic = PlotWidget(self.tabHrvDfa)
       # self.hrvdfa_graphic = QtWidgets.QGraphicsView(self.tabHrvDfa)
        self.hrvdfa_graphic.setGeometry(QtCore.QRect(150, 20, 521, 361))
        self.hrvdfa_graphic.setObjectName("hrvdfa_graphic")


        self.HRV_DFA_table = QtWidgets.QTableWidget(self.tabHrvDfa)
        self.HRV_DFA_table.setGeometry(QtCore.QRect(10, 60, 136, 85))
        self.HRV_DFA_table.setObjectName("HRV_DFA_table")
        self.HRV_DFA_table.setColumnCount(1)
        self.HRV_DFA_table.setRowCount(2)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_DFA_table.setVerticalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_DFA_table.setVerticalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_DFA_table.setHorizontalHeaderItem(0, item)

       # self.run_HRV_DFA_BUTTON = QtWidgets.QPushButton(self.tabHrvDfa)
       # self.run_HRV_DFA_BUTTON.setGeometry(QtCore.QRect(10, 160, 120, 61))
       # self.run_HRV_DFA_BUTTON.setObjectName("run_HRV_DFA_BUTTON")
        self.hrv_widget.addTab(self.tabHrvDfa, "")
        self.tabHrv1 = QtWidgets.QWidget()
        self.tabHrv1.setObjectName("tabHrv1")
       # self.hrv1_graphic = QtWidgets.QGraphicsView(self.tabHrv1)
        self.hrv1_graphic = PlotWidget(self.tabHrv1)
        self.hrv1_graphic.setGeometry(QtCore.QRect(300, 40, 371, 331))
        self.hrv1_graphic.setObjectName("hrv1_graphic")

        self.HRV_1_table = QtWidgets.QTableWidget(self.tabHrv1)
        self.HRV_1_table.setGeometry(QtCore.QRect(20, 20, 225, 351))
        self.HRV_1_table.setMouseTracking(False)
        self.HRV_1_table.setAutoFillBackground(True)
        self.HRV_1_table.setFrameShape(QtWidgets.QFrame.VLine)
        self.HRV_1_table.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.HRV_1_table.setLineWidth(1)
        self.HRV_1_table.setMidLineWidth(0)
        self.HRV_1_table.setAlternatingRowColors(True)
        self.HRV_1_table.setShowGrid(True)
        self.HRV_1_table.setGridStyle(QtCore.Qt.SolidLine)
        self.HRV_1_table.setObjectName("HRV_1_table")
        self.HRV_1_table.setColumnCount(1)
        self.HRV_1_table.setRowCount(14)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_1_table.setVerticalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_1_table.setVerticalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_1_table.setVerticalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_1_table.setVerticalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_1_table.setVerticalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_1_table.setVerticalHeaderItem(5, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_1_table.setVerticalHeaderItem(6, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_1_table.setVerticalHeaderItem(7, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_1_table.setVerticalHeaderItem(8, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_1_table.setVerticalHeaderItem(9, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_1_table.setVerticalHeaderItem(10, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_1_table.setVerticalHeaderItem(11, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_1_table.setVerticalHeaderItem(12, item)
        item = QtWidgets.QTableWidgetItem()
        self.HRV_1_table.setVerticalHeaderItem(13, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        font.setStrikeOut(False)
        item.setFont(font)
        self.HRV_1_table.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        item.setForeground(brush)
        self.HRV_1_table.setItem(0, 0, item)
        self.HRV_1_table.horizontalHeader().setCascadingSectionResizes(False)
        self.HRV_1_table.verticalHeader().setCascadingSectionResizes(False)
       # self.run_HRV1_BUTTON = QtWidgets.QPushButton(self.tabHrv1)
       # self.run_HRV1_BUTTON.setGeometry(QtCore.QRect(260, 100, 31, 161))
       # self.run_HRV1_BUTTON.setObjectName("run_HRV1_BUTTON")
        self.hrv_widget.addTab(self.tabHrv1, "")

        self.tabHrv2 = QtWidgets.QWidget()
        self.tabHrv2.setObjectName("tabHrv2")
        #self.hrv2_graphic = QtWidgets.QGraphicsView(self.tabHrv2)
        self.hrv2_graphic = PlotWidget(self.tabHrv2)
        self.hrv2_graphic.setGeometry(QtCore.QRect(10, 20, 671, 301))
        self.hrv2_graphic.setObjectName("hrv2_graphic")
        #self.hrv2_graphicHistogram = PlotWidget(self.tabHrv2)
        #self.hrv2_graphicHistogram.setGeometry(QtCore.QRect(20, 40, 310, 331))
        #self.hrv2_graphicHistogram.setObjectName("hrv2_graphic")
        #self.comboBox_hrv2 = QtWidgets.QComboBox(self.tabHrv2)
        #self.comboBox_hrv2.setGeometry(QtCore.QRect(10, 20, 671, 21))

        #self.comboBox_hrv2.setObjectName("comboBox_3")
        #self.comboBox_hrv2.addItem("")
        #self.comboBox_hrv2.addItem("")
        #self.run_HRV2_BUTTON = QtWidgets.QPushButton(self.tabHrv2)
        #self.run_HRV2_BUTTON.setGeometry(QtCore.QRect(210, 330, 291, 61))
        #self.run_HRV2_BUTTON.setObjectName("run_HRV2_BUTTON")
        self.hrv_widget.addTab(self.tabHrv2, "")
        self.tabWidget.addTab(self.tabHrv, "")

        self.tabHeart = QtWidgets.QWidget()
        self.tabHeart.setObjectName("tabHeart")
        self.verticalLayoutWidget_5 = QtWidgets.QWidget(self.tabHeart)
        self.verticalLayoutWidget_5.setGeometry(QtCore.QRect(20, 359, 333, 111))
        self.verticalLayoutWidget_5.setObjectName("verticalLayoutWidget_5")
        self.heartLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_5)
        self.heartLayout.setContentsMargins(0, 0, 0, 0)
        self.heartLayout.setObjectName("heartLayout")
       # self.pushButton_8 = QtWidgets.QPushButton(self.verticalLayoutWidget_5)
       # self.pushButton_8.setObjectName("pushButton_8")
      #  self.heartLayout.addWidget(self.pushButton_8)
        self.Heart_Class_table = QtWidgets.QTableWidget(self.tabHeart)
        self.Heart_Class_table.setGeometry(QtCore.QRect(150, 360, 358, 111))
        self.Heart_Class_table.setObjectName("Heart_Class_table")
        self.Heart_Class_table.setColumnCount(1)
        self.Heart_Class_table.setRowCount(3)
        item = QtWidgets.QTableWidgetItem()
        self.Heart_Class_table.setVerticalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.Heart_Class_table.setVerticalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.Heart_Class_table.setVerticalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.Heart_Class_table.setHorizontalHeaderItem(0, item)
        self.verticalLayoutWidget_8 = QtWidgets.QWidget(self.tabHeart)
        self.verticalLayoutWidget_8.setGeometry(QtCore.QRect(20, 40, 650, 311))
        self.verticalLayoutWidget_8.setObjectName("verticalLayoutWidget_8")
        self.heartLayoutvis = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_8)
        self.heartLayoutvis.setContentsMargins(0, 0, 0, 0)
        self.heartLayoutvis.setObjectName("heartLayoutvis")
        self.label_10 = QtWidgets.QLabel(self.verticalLayoutWidget_8)
        self.label_10.setObjectName("label_10")
        self.heartLayoutvis.addWidget(self.label_10)
        self.heartclass_graphic = PlotWidget(self.verticalLayoutWidget_8)
        #self.heartclass_graphic = QtWidgets.QGraphicsView(self.verticalLayoutWidget_8)
        self.heartclass_graphic.setObjectName("heartclass_graphic")

        self.heartclass_graphic2 = PlotWidget(self.verticalLayoutWidget_8)
        self.heartLayoutvis.addWidget(self.heartclass_graphic)
       # self.heartclass_graphic2 = QtWidgets.QGraphicsView(self.verticalLayoutWidget_8)
        self.heartclass_graphic2.setObjectName("heartclass_graphic")

        self.heartLayoutvis.addWidget(self.heartclass_graphic2)
        self.tabWidget.addTab(self.tabHeart, "")
        self.verticalLayout.addWidget(self.tabWidget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 490, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        self.hrv_widget.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        ######################################
        
        self.Segmentlayoutvis.addWidget(self.tbl)

        header_labels = ['Time', 'Offset (mV)', 'Slope (°)', 'Episode', 'Shape']
        self.tbl.setHorizontalHeaderLabels(header_labels)
        #for row in self.all_data:
        #    inx = self.all_data.index(row)
        #    self.tbl.insertRow(inx)
        #    self.tbl.setItem(inx, 0, QtWidgets.QTableWidgetItem(str(row[0])))
        #    self.tbl.setItem(inx, 1, QtWidgets.QTableWidgetItem(str(row[1])))
        #    self.tbl.setItem(inx, 2, QtWidgets.QTableWidgetItem(str(row[2])))
        #    self.tbl.setItem(inx, 3, QtWidgets.QTableWidgetItem(str(row[3])))
        header = self.tbl.horizontalHeader()

        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(3, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(4, QtWidgets.QHeaderView.Stretch)
        self.Segmentlayoutvis.addWidget(self.tbl)
        #######################################


    def retranslateUi(self, MainWindow):

        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        MainWindow.setFixedWidth(800)
        MainWindow.setFixedHeight(700)
        self.dadmprojektLABEL.setText(_translate("MainWindow", "DADM PROJECT "))
        self.tbl.setFixedHeight(170)
        # PATH 1 linijka 17.01
        self.PATH.setText(_translate("MainWindow", "Path"))

        self.ButtonReadsignal.setText(_translate("MainWindow", "Load signal"))

        # dodanie zakresu 17.01
        self.ODLABEL.setText(_translate("MainWindow", "FROM"))
        self.DOLABEL.setText(_translate("MainWindow", "TO"))
        self.button_dodajzakres.setText(_translate("MainWindow", "Load fragment"))

        self.radioButton.setText(_translate("MainWindow", "R peaks"))
        self.label_2.setText(_translate("MainWindow", "Choose filter:"))
        self.comboBox.setItemText(0, _translate("MainWindow", "Butterworth"))
        self.comboBox.setItemText(1, _translate("MainWindow", "SavGol"))
        self.comboBox.setItemText(2, _translate("MainWindow", "Moving avg"))
        self.buttonECGbaseline.setText(_translate("MainWindow", "Analyze signal"))
        #self.buttonECGbaseline.clicked.connect(lambda:self.ECG_plot())
        self.label_3.setText(_translate("MainWindow", "Choose algorythm:"))
        self.comboBoxTompkins.setItemText(0, _translate("MainWindow", "Pan-Tompkins"))
        self.comboBoxTompkins.setItemText(1, _translate("MainWindow", "Hilbert"))
      #  self.buttonRpeaks.setText(_translate("MainWindow", "Analyze signal"))
        #self.buttonECGbaseline.clicked.connect(lambda: self.Peaks_plot())
        self.label_4.setText(_translate("MainWindow", "Visualisation ECG/R peaks"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("MainWindow", "ECG and Peaks"))
       # self.pushButton_5.setText(_translate("MainWindow", "Read signal"))
        #self.buttonQRS.setText(_translate("MainWindow", "Run ECG with characteristic points"))
        self.buttonECGbaseline.clicked.connect(lambda: self.start_analyze())
        #self.buttonECGbaseline.clicked.connect(lambda: self.Segment_plot())
        #self.buttonECGbaseline.clicked.connect(lambda: self.HRVDFA_plot())
        #self.buttonECGbaseline.clicked.connect(lambda: self.HRV1_plot())
        #self.buttonECGbaseline.clicked.connect(lambda: self.HRV2_plot())
        #self.buttonECGbaseline.clicked.connect(lambda: self.HEART_plot())
        #self.buttonECGbaseline.clicked.connect(lambda: self.tables())

        self.button_dodajzakres.clicked.connect(lambda: self.dodajzakres_plot())
        self.ButtonReadsignal.clicked.connect(lambda: self.readsignal_plot())
        #self.buttonECGbaseline.clicked.connect(lambda: self.tables())
        #radio mozna uzyc z
        self.radioButton.toggled.connect(lambda: self.Peaks_plot())
        #lub z
        self.radioButton.isChecked()
        self.labelQRS.setText(_translate("MainWindow", "Visualisation QRS"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabWaves), _translate("MainWindow", "Waves"))

        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabSegment), _translate("MainWindow", "ST_SEGMENT"))
       # self.pushButton_8.setText(_translate("MainWindow", "RUN"))

        item = self.HRV_DFA_table.verticalHeaderItem(0)
        item.setText(_translate("MainWindow", "alfa1"))
        item = self.HRV_DFA_table.verticalHeaderItem(1)
        item.setText(_translate("MainWindow", "alfa2"))
        item = self.HRV_DFA_table.horizontalHeaderItem(0)
        self.HRV_DFA_table.setFixedHeight(81)
        self.HRV_DFA_table.setFixedWidth(120)
        #self.HRV_DFA_table.item(0, 1).setText("Put here whatever you want!")

        item.setText(_translate("MainWindow", "Value"))


       # self.run_HRV_DFA_BUTTON.setText(_translate("MainWindow", "Run"))
        self.hrv_widget.setTabText(self.hrv_widget.indexOf(self.tabHrvDfa), _translate("MainWindow", "HRV_DFA"))
        self.hrv_widget.setTabText(self.hrv_widget.indexOf(self.tabHrv1), _translate("MainWindow", "HRV1"))
        item = self.HRV_1_table.verticalHeaderItem(0)
        item.setText(_translate("MainWindow", "RR [ms]"))
        item = self.HRV_1_table.verticalHeaderItem(1)
        item.setText(_translate("MainWindow", "SDNN [ms]"))
        item = self.HRV_1_table.verticalHeaderItem(2)
        item.setText(_translate("MainWindow", "RMSSD [ms]"))
        item = self.HRV_1_table.verticalHeaderItem(3)
        item.setText(_translate("MainWindow", "NN50 [-]"))
        item = self.HRV_1_table.verticalHeaderItem(4)
        item.setText(_translate("MainWindow", "pNN50 [%]"))
        item = self.HRV_1_table.verticalHeaderItem(5)
        item.setText(_translate("MainWindow", "SDANN [ms]"))
        item = self.HRV_1_table.verticalHeaderItem(6)
        item.setText(_translate("MainWindow", "SDANN index [ms]"))
        item = self.HRV_1_table.verticalHeaderItem(7)
        item.setText(_translate("MainWindow", "SDSD [ms]"))
        item = self.HRV_1_table.verticalHeaderItem(8)
        item.setText(_translate("MainWindow", "TP [ms^2]"))
        item = self.HRV_1_table.verticalHeaderItem(9)
        item.setText(_translate("MainWindow", "ULV [ms^2]"))
        item = self.HRV_1_table.verticalHeaderItem(10)
        item.setText(_translate("MainWindow", "VLF [ms^2]"))
        item = self.HRV_1_table.verticalHeaderItem(11)
        item.setText(_translate("MainWindow", "LF [ms^2]"))
        item = self.HRV_1_table.verticalHeaderItem(12)
        item.setText(_translate("MainWindow", "HF [ms^2]"))
        item = self.HRV_1_table.verticalHeaderItem(13)
        item.setText(_translate("MainWindow", "LFHF [-]"))
        item = self.HRV_1_table.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Value"))
        __sortingEnabled = self.HRV_1_table.isSortingEnabled()
        self.HRV_1_table.setSortingEnabled(False)
        self.HRV_1_table.setSortingEnabled(__sortingEnabled)
       # self.run_HRV1_BUTTON.setText(_translate("MainWindow", "Run"))
        self.hrv_widget.setTabText(self.hrv_widget.indexOf(self.tabHrv2), _translate("MainWindow", "HRV2"))
        #self.comboBox_hrv2.setItemText(0, _translate("MainWindow", "Wykres Poincare"))
        #self.comboBox_hrv2.setItemText(1, _translate("MainWindow", "Histogram"))
       # self.run_HRV2_BUTTON.setText(_translate("MainWindow", "Run"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabHrv), _translate("MainWindow", "HRV"))
       # self.buttonSegmentRun.setText(_translate("MainWindow", "Run"))

        self.label_10.setText(_translate("MainWindow", "Visualisation of classification"))
        item = self.Heart_Class_table.verticalHeaderItem(0)
        item.setText(_translate("MainWindow", "Supraventricular beats"))
        item = self.Heart_Class_table.verticalHeaderItem(1)
        item.setText(_translate("MainWindow", "Ventricular beats"))
        item = self.Heart_Class_table.verticalHeaderItem(2)
        item.setText(_translate("MainWindow", "Unclassified beats/Artifacts"))
        item = self.Heart_Class_table.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Value"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabHeart), _translate("MainWindow", "HEART_CLASS"))


def gui_start(q_to_gui: queue.Queue, q_to_main: queue.Queue):

    def watch_q():

        if (ui.analyzing == 1):
            ui.dots = ui.dots + "."
            if (ui.dots == "......................."):
                ui.dots = "."
            ui.POLEDOSCIEZKI.setText("Analyzing" + ui.dots)

        try:
            value = q_to_gui.get(block=False)
        except queue.Empty:
            return
        else:
            if (value["command"] == "update_ecg_rr_wv_st"):
                ui.Wizualizacja.add_ECG_plot(value["data"]["ecg"], ui.fs, ui.off)
                ui.wave_graphic.add_ECG_plot(value["data"]["ecg"], ui.fs, ui.off)
                ui.Segmentacja.add_ECG_plot(value["data"]["ecg"], ui.fs, ui.off)
                #print("data received")
                ui.r_peaks = value["data"]["r_peaks"]
                ui.wave_graphic.plot_widget.plot_waves(value["data"]["waves"])
                ui.Segmentacja.plot_widget.plot_ST(value["data"]["st_segment"])
                ui.st_table(value["data"]["st_segment"])

            elif (value["command"] == "update_metadata"):
                #print("metadata received")
                ui.fs = value["data"]["fs"]
                ui.POLEDOSCIEZKI.setText("Sig loaded correctly")

            elif (value["command"] == "error"):
                if (value["data"] == "No filepath"):
                    ui.POLEDOSCIEZKI.setText("No filepath")

                elif (value["data"] == "Wrong filepath"):
                    ui.POLEDOSCIEZKI.setText("Wrong filepath")

            elif (value["command"] == "analysis_done"):
                ui.analyzing = 0
                ui.no_of_samples = value["data"]["num_of_samples"]
                ui.POLEDOSCIEZKI.setText("sig analyzed, size: " + str(int(value["data"]["num_of_samples"])))
                ui.tables(value["data"]["hrv1"], value["data"]["hrv_dfa"], value["data"]["heart_class"])
                ui.HRV2_plot(value["data"]["hrv2"])
                ui.HRV1_plot(value["data"]["hrv1"]["periodogram"])
                ui.HRVDFA_plot(value["data"]["hrv_dfa"])
                ui.HEART_plot(value["data"]["heart_class"])
                
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    app.setStyle("Fusion")
    ui = Ui_MainWindow(q_to_main)
    ui.setupUi(MainWindow)
    MainWindow.show()

    #timer
    timer = QTimer()
    timer.timeout.connect(watch_q)
    timer.setInterval(15)  
    timer.start()

    app.exec_()

    timer.stop()

    q_to_main.put({
	    "command": "exit"
	    })


    