from wfdb import rdheader, rdsamp
import numpy as np

def get_file_props(filepath: str):
    """
    Funkcja pobiera z pliku nazwy odprowadzeń i jego długość

    Args:
        filepath (str): scieżka do pliku bez rozszerzenia

    Returns:
        fileProps (dict): słownik zawierający pola:
            signals (list(str)): nazwy odprowadzeń
            samples (int): długosc pliku w próbkach

    Raises:
        ???
    """
    file_props = rdheader(filepath, pn_dir=None, rd_segments=False)
    signals, fields = rdsamp(filepath, channels=[0], sampfrom = 0, sampto = 1)
    fileprops = {
        "signals": file_props.sig_name,
        "samples": file_props.sig_len,
        "fs": fields["fs"]
    }
    return (fileprops)


def read_file(filepath: str, channel: int, samples_from_to: [int,int], redundant_samples: [int,int]):
    """
    Wczytuje paczkę danych do analizy z pliku i zwraca sygnał oraz jego podstawowe parametry

    Args:
        filepath (string): scieżka do pliku bez rozszerzenia
        channel (int): numer wyprowadzenia
        samples_from_to (list [int, int]): dwuelementowa lista zawierająca liczby całkowite reprezentujące początek i koniec paczki danych do analizy
        redundant_samples: (list [int, int]):dwuelementowa lista zawierająca liczby całkowite reprezentujące ilość dodatkowych próbek, 
            pierwsza liczba oznacza dodatkowe próbki przed i po do filtrowania sygnału, druga liczba oznacza dodatkowe próbki przed i po do reszty modułów

    Returns:
        (dict): słownik zawierający pola:
            signal (list(float): sygnał
            fs (int): częstotliwość próbkowania
            len (int): ilość wczytanych próbek razem z redundancją
            name (str): nazwa odprowadzenia 
            sample_range (list [int, int]): patrz Args -> samples_from_to
            redundancy (list [int, int]): patrz Args -> redundant_samples

    Raises:
        ???
    """
    signals, fields = rdsamp(filepath, channels=[channel], sampfrom = samples_from_to[0] - redundant_samples[0] - redundant_samples[1], sampto = samples_from_to[1] + redundant_samples[0] + redundant_samples[1])
    
    return {
        "signal": signals.T[0],
        "fs": fields["fs"],
        "len": fields["sig_len"],
        "name": fields["sig_name"],
        "sample_range": samples_from_to,
        "redundancy": redundant_samples
        }

def cut_redundant_1(signal: dict, ecgbas: np.array):
    l = range(signal["redundancy"][0])
    a = np.asarray(l)+1
    b = (np.asarray(l)+1)*(-1)
    ab = np.concatenate([a,b])

    #deleting samples from raw signal
    signal["signal"] = np.delete(signal["signal"], ab)

    #deleting samples from baseline
    ecgbas = np.delete(ecgbas, ab)

    #setting proper len
    signal["len"] = signal["len"] - signal["redundancy"][0]*2

    return signal, ecgbas

def cut_redundant_2(data_container: dict):
    l = range(data_container["loaded_signal"]["redundancy"][1])
    a = np.asarray(l)+1
    b = (np.asarray(l)+1)*(-1)
    ab = np.concatenate([a,b])

    #deleting samples from raw signal
    data_container["loaded_signal"]["signal"] = np.delete(data_container["loaded_signal"]["signal"], ab)

    #deleting samples from baseline
    data_container["ecg_baseline"] = np.delete(data_container["ecg_baseline"], ab)

    #setting proper len
    data_container["loaded_signal"]["len"] = data_container["loaded_signal"]["len"] - data_container["loaded_signal"]["redundancy"][1]*2

    #deleting rpeaks detected on redundant samples and offsetting them after deletion
    data_container["r_peaks"] = data_container["r_peaks"][(data_container["r_peaks"] >= data_container["loaded_signal"]["redundancy"][1]) & (data_container["r_peaks"] < data_container["loaded_signal"]["redundancy"][1] + data_container["loaded_signal"]["len"])]
    data_container["r_peaks"] = data_container["r_peaks"] - data_container["loaded_signal"]["redundancy"][1]

    #deleting waves values on redundant samples and offsetting them after deletion
    #qrs_onsets
    data_container["waves"]["QRS_onsets"] = data_container["waves"]["QRS_onsets"][(data_container["waves"]["QRS_onsets"] >= data_container["loaded_signal"]["redundancy"][1]) & (data_container["waves"]["QRS_onsets"] < data_container["loaded_signal"]["redundancy"][1] + data_container["loaded_signal"]["len"])]
    data_container["waves"]["QRS_onsets"] = data_container["waves"]["QRS_onsets"] - data_container["loaded_signal"]["redundancy"][1]

    #QRS_ends
    data_container["waves"]["QRS_ends"] = data_container["waves"]["QRS_ends"][(data_container["waves"]["QRS_ends"] >= data_container["loaded_signal"]["redundancy"][1]) & (data_container["waves"]["QRS_ends"] < data_container["loaded_signal"]["redundancy"][1] + data_container["loaded_signal"]["len"])]
    data_container["waves"]["QRS_ends"] = data_container["waves"]["QRS_ends"] - data_container["loaded_signal"]["redundancy"][1]

    #T_ends
    data_container["waves"]["T_ends"] = data_container["waves"]["T_ends"][(data_container["waves"]["T_ends"] >= data_container["loaded_signal"]["redundancy"][1]) & (data_container["waves"]["T_ends"] < data_container["loaded_signal"]["redundancy"][1] + data_container["loaded_signal"]["len"])]
    data_container["waves"]["T_ends"] = data_container["waves"]["T_ends"] - data_container["loaded_signal"]["redundancy"][1]

    #P_insets
    data_container["waves"]["P_onsets"] = data_container["waves"]["P_onsets"][(data_container["waves"]["P_onsets"] >= data_container["loaded_signal"]["redundancy"][1]) & (data_container["waves"]["P_onsets"] < data_container["loaded_signal"]["redundancy"][1] + data_container["loaded_signal"]["len"])]
    data_container["waves"]["P_onsets"] = data_container["waves"]["P_onsets"] - data_container["loaded_signal"]["redundancy"][1]

    #P_ends
    data_container["waves"]["P_ends"] = data_container["waves"]["P_ends"][(data_container["waves"]["P_ends"] >= data_container["loaded_signal"]["redundancy"][1]) & (data_container["waves"]["P_ends"] < data_container["loaded_signal"]["redundancy"][1] + data_container["loaded_signal"]["len"])]
    data_container["waves"]["P_ends"] = data_container["waves"]["P_ends"] - data_container["loaded_signal"]["redundancy"][1]

    #deleting st segment

    data_container["st_segment"]["ST_ends"] = data_container["st_segment"]["ST_ends"][(data_container["st_segment"]["ST_starts"] >= data_container["loaded_signal"]["redundancy"][1]) & (data_container["st_segment"]["ST_starts"] < data_container["loaded_signal"]["redundancy"][1] + data_container["loaded_signal"]["len"])]
    data_container["st_segment"]["ST_ends"] = data_container["st_segment"]["ST_ends"] - data_container["loaded_signal"]["redundancy"][1]

    data_container["st_segment"]["offsets"] = data_container["st_segment"]["offsets"][(data_container["st_segment"]["ST_starts"] >= data_container["loaded_signal"]["redundancy"][1]) & (data_container["st_segment"]["ST_starts"] < data_container["loaded_signal"]["redundancy"][1] + data_container["loaded_signal"]["len"])]

    data_container["st_segment"]["slopes"] = data_container["st_segment"]["slopes"][(data_container["st_segment"]["ST_starts"] >= data_container["loaded_signal"]["redundancy"][1]) & (data_container["st_segment"]["ST_starts"] < data_container["loaded_signal"]["redundancy"][1] + data_container["loaded_signal"]["len"])]
    
    data_container["st_segment"]["episodes"] = data_container["st_segment"]["episodes"][(data_container["st_segment"]["ST_starts"] >= data_container["loaded_signal"]["redundancy"][1]) & (data_container["st_segment"]["ST_starts"] < data_container["loaded_signal"]["redundancy"][1] + data_container["loaded_signal"]["len"])]
    
    data_container["st_segment"]["shape_episodes"] = data_container["st_segment"]["shape_episodes"][(data_container["st_segment"]["ST_starts"] >= data_container["loaded_signal"]["redundancy"][1]) & (data_container["st_segment"]["ST_starts"] < data_container["loaded_signal"]["redundancy"][1] + data_container["loaded_signal"]["len"])]

    data_container["st_segment"]["ST_starts"] = data_container["st_segment"]["ST_starts"][(data_container["st_segment"]["ST_starts"] >= data_container["loaded_signal"]["redundancy"][1]) & (data_container["st_segment"]["ST_starts"] < data_container["loaded_signal"]["redundancy"][1] + data_container["loaded_signal"]["len"])]
    data_container["st_segment"]["ST_starts"] = data_container["st_segment"]["ST_starts"] - data_container["loaded_signal"]["redundancy"][1]


    return data_container