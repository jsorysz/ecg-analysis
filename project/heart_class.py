from typing import List, Dict
import numpy 
from joblib import load
import collections  
import sklearn
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score

class features_extractor:
    '''
    Helper class for heart_class that is resposible for calculating vectors of features 
    for QRS fragements.
    '''

    def __init__(self, precision: int = 2):
        '''
        Constructor of an object of features_extractor.

        Args:
            precision (int): parameter that defines numbers of digits after comma 
            for calculated features
        '''
        self.precision = precision

    def get_features_vectors(self, fragments: List[numpy.ndarray]) -> List[List[float]]:
        '''
        Main class method responsible for launching calculation process and returning list of
        features' vectors.

        Args:
            fragments(List[numpy.ndarray]): list of QRS fragments

        Returns:
            List[List[float]]: list of calculated vectors.
        '''
        return [self.__form_features_vector(fragment) for fragment in fragments]
    
    def __form_features_vector(self, fragment: numpy.ndarray) -> List[float]:
        '''
        Form vector of features for single fragment passed on input.

        Args:
            fragment(numpy.ndarray): QRS fragment

        Returns:
            List[float]: list of three calculated features.
        '''
        feature_1 = self.__calculate_shape_factor(fragment)
        feature_2 = self.__calculate_max_velocity_to_max_amplitude_ratio(fragment)
        feature_3 = self.__calculate_number_of_samples_in_which_velocity_exceeds_40pct_of_max_velocity(fragment)
        return [feature_1, feature_2, feature_3]

    def __calculate_shape_factor(self, fragment: numpy.ndarray) -> float:
        '''
        Calculate QRS fragment shape factor.

        Args:
            fragment(numpy.ndarray): QRS fragment

        Returns:
            float: value of shape factor, 
            -1 is returned in case 0 is obtained on denominator
        '''
        numerator = sum(abs(fragment))
        denominator = 0
        for i in range(1,len(fragment)):
            denominator += abs(fragment[i]-fragment[i-1])
        if(denominator == 0):
            return float(-1)
        else:
            return round(float(10 * (numerator/denominator)),self.precision)

    def __calculate_max_velocity_to_max_amplitude_ratio(self, fragment: numpy.ndarray) -> float:
        '''
        Calculate ratio of maximum velocity to maximum amplitude in QRS fragment.

        Args:
            fragment(numpy.ndarray): QRS fragment

        Returns:
            float: value of ratio of maximum velocity to maximum amplitude,
            -1 is returned in case 0 is obtained on denominator
        '''
        possible_numerator = []
        for i in range(2,len(fragment)):
            possible_numerator.append(abs(fragment[i] + fragment[i-2] - 2*fragment[i-1]))    
        if(len(possible_numerator) != 0):
            numerator = max(possible_numerator)
        else:
            return float(0)
        denominator = abs(max(fragment[2:]) - min(fragment[2:]))
        if(denominator == 0):
            return float(-1)
        else:
            return round(float(1000*numerator/denominator),self.precision)

    def __calculate_number_of_samples_in_which_velocity_exceeds_40pct_of_max_velocity(self, fragment: numpy.ndarray) -> int:
        '''
        Calculate number of samples in which velocity exceeds 40 % of maximum 
        velocity in QRS fragment.

        Args:
            fragment(numpy.ndarray): QRS fragment

        Returns:
            float: number of samples
        '''
        possbile_velocities = []
        for i in range(1,len(fragment)):
            possbile_velocities.append(abs(fragment[i]-fragment[i-1]))
        if (len(possbile_velocities) != 0):
            max_40_percent = 0.4 * max(possbile_velocities) #nowe 
        more_than_40_percent = [value for value in possbile_velocities if value > max_40_percent]
        return float(len(more_than_40_percent))


class labels_handler:
    '''
    Helper class for heart_class that is resposible for decription of labels 
    returned by pretrained k-NN model.
    '''

    labels_translation = { 
        "Supraventricular beat": 0,
        "Unclassified beat/Artifact": 1,
        "Ventricular beat": 2
    }

    def report_number_of_each_label(self, decrypted_labels: List[int]) -> Dict:
        '''
        Calculate number of each label in predicted list. 

        Args:
            decrypted_labels: List[int]: list of labels returned by model

        Returns:
            Dict: report of quantity of each class, keys are classes' names and
            values are corresponding quantities
        '''
        return { 
            "Supraventricular beat": collections.Counter(decrypted_labels)[0],
            "Unclassified beat/Artifact": collections.Counter(decrypted_labels)[1],
            "Ventricular beat": collections.Counter(decrypted_labels)[2]
        }

    def derive_indexes_of_label(self, decrypted_labels: List[int], label: int):
        '''
        Extract values from decrypted_labels list that are placed under desired indexes.

        Args:
            decrypted_labels (List): list of labels
            indexes: (List[float]): list of desired indexes

        Returns:
            (numpy.ndarray): array that contains values from desired indexes
        '''
        return numpy.where(numpy.array(decrypted_labels) == label)[0]


class cluster_handler:
    '''
    Helper class for heart_class that is resposible for clustering vectors of features.
    Silhouette analysis is performed based on results of k-means clustering. It finds the best
    number of clusters to describe given data.
    '''
    __min_number_of_clusters = 2
    __max_number_of_clusters = 3

    def perform_k_means_with_undefined_num_of_clusters(self, data: List):
        '''
        Trigger silhouette analysis and perform k-means for best k value.

        Args:
            data (List): list of features vectors

        Returns:
            (numpy.ndarray): array that contains label for each vector that
            informs about its cluster
            (int): established number of clusters
        '''
        best_clusters_num = self.__perform_silhouette_analysis(data)
        if(best_clusters_num == 1):
            return list(numpy.zeros(len(data))), 1
        else:
            kmeans = self.__fit_k_means(data, best_clusters_num)
            return kmeans.predict(data), best_clusters_num

    def __perform_silhouette_analysis(self, data: List):
        '''
        Perform silhouette analysis for given data.

        Args:
            data (List): list of features vectors

        Returns:
            (int): number of clusters for which silhouette coefficient
            has the highest value (best clustering)
        '''
        silhouette_avg = []
        possible_number_of_classes = len(set(tuple(vector) for vector in data))
        if(possible_number_of_classes > 2):
            if(len(data) < cluster_handler.__max_number_of_clusters):
                upper_limitation = len(data) 
            else:
                upper_limitation = cluster_handler.__max_number_of_clusters 
            poss_num_of_clusters = list(range(cluster_handler.__min_number_of_clusters, upper_limitation, 1))
            for num_clusters in poss_num_of_clusters:
                kmeans = self.__fit_k_means(data, num_clusters)
                cluster_labels = kmeans.labels_
                silhouette_avg.append(silhouette_score(data, cluster_labels))
            best_avg = max(silhouette_avg)
            for i, avg in enumerate(silhouette_avg):
                if(avg == best_avg):
                    return poss_num_of_clusters[i]
        else:
            if(possible_number_of_classes == cluster_handler.__min_number_of_clusters):
                return possible_number_of_classes
            else:
                return 1
    
    def __fit_k_means(self, data: List, num_of_clusters: int):
        '''
        Perform k-means clustering.

        Args:
            data (List): list of features vectors
            num_of_clusters (int): number of clusters for data division

        Returns:
            (numpy.ndarray): array that contains label for each vector that
            informs about its cluster
        '''
        kmeans = KMeans(n_clusters=num_of_clusters)
        return kmeans.fit(data)
        

class heart_class_result:
    '''
    Helper class for heart_class that is resposible for packing output data.
    This class is mostly dedicated for visualization modules. It exposes 
    calculeted data in more transparent way.
    Class provides all the data required for charts and class quantities v
    visualization.
    '''

    def __init__(self, labels_report: Dict, chart_data_1: List, chart_data_2: List):
        '''
        Constructor of an object of heart_class_result. Used only by heart_class
        to build object for output.

        Args:
            labels_report (Dict): report dectionary that contains quantity of each 
            classifiantion class
            chart_data_1 (List): data to plot on 1st chart ('Supraventricular beat')    
            chart_data_2 (List): data to plot on 2nd chart ('Ventricular beat')   
        '''
        self.labels_report = labels_report
        self.chart_data_1 = chart_data_1
        self.chart_data_2 = chart_data_2
        self.labels_chart_1 = self.__generate_labels(len(chart_data_1))
        self.labels_chart_2 = self.__generate_labels(len(chart_data_2))
        self.number_of_clusters_1 = len(chart_data_1)
        self.number_of_clusters_2 = len(chart_data_2)

    def get_axis_for_index(self, chart_num: int, axis: str, index: int):
        '''
        Returns data for desired chart ('Supraventricular beat' or 'Ventricular beat')
        for desired axis (X,Y,Z) for desired cluster number.

        Args:
            chart_num (int): chart number; 1 - 'Supraventricular beat' or 2 -'Ventricular beat'
            axis (str): axis name; possibilites: 'X', 'Y', 'Z'
            index (int): index of clusters which are currently plotted; range: [0, max],
            (max value is returned by 'get_number_of_clusters_1' or 'get_number_of_clusters_2'
            methods)

        Returns:
            (list): X, Y or Z data coordinates to plot
        '''
        if(chart_num == 1):
            data = self.chart_data_1
        else:
            data = self.chart_data_2
        if(axis == "X"):
            return [first for [first, second, third] in data[index]]
        if(axis == "Y"):
            return [second for [first, second, third] in data[index]]
        if(axis == "Z"):
            return [third for [first, second, third] in data[index]]

    def get_labels_report(self):
        '''
        Returns quantity of representatives of each class: 'Supraventricular beat', 'Ventricular beat'
        and "Unclassified beat/Artifact".

        Returns:
            (Dict[str, int]): quantitiy of each class represntative 
        '''
        return self.labels_report 
    
    def get_labels_1(self):
        '''
        Returns labels used in legend in 1st plot.

        Returns:
            (List[str]): list of labels as strings 
        '''
        return self.labels_chart_1
    
    def get_labels_2(self):
        '''
        Returns labels used in legend in 2nd plot.

        Returns:
            (List[str]): list of labels as strings 
        '''
        return self.labels_chart_2
    
    def get_number_of_clusters_1(self):
        '''
        Returns number of clusters that are used in 1st plot. 

        Returns:
            (int): number of clusters
        '''
        return self.number_of_clusters_1
    
    def get_number_of_clusters_2(self):
        '''
        Returns number of clusters that are used in 2nd plot. 

        Returns:
            (int): number of clusters
        '''
        return self.number_of_clusters_2
    
    def __generate_labels(self, num_of_labels: int):
        '''
        Inner function to generate labels that are lately used by 
        get_labels_1 and get_labels_2. 

        Args:
            num_of_labels (int): number of clusters in which data is splitted

        Returns:
            (int): number of clusters
        '''
        labels = []
        for val in range(num_of_labels):
            labels.append('Cluster {}'.format(val+1))
        return labels


class heart_class:
    '''
    Class for analyzing QRS fragemnt of ECG. It calculates vectors of features and
    classifies QRS to one of three categories: 'Supraventricular beat', 'Unclassified
    beat/Artifact', 'Ventricular beat' with use of pretrained k-NN algorithm. Class
    also returns data for clustering plots. Supraventricular and Ventricular beats are
    grouped into smaller subgroups with use of K-means algorithm and Silhouette analysis.
    Obtained values are packed into heart_class_result for.

    Attributes:
        baseline: Numpy.ndarray containing filtered ECG signal.
        waves: Dict containing indexes of QRS fragments.
        heart_class_result: heart_class_result object that contains module's results.

    Example:
        heart_class = heart_class(ecg, waves_dict)
        heart_class_result_example = heart_class.run()
    '''

    __fe = features_extractor()
    __lh = labels_handler()
    __ch = cluster_handler()
    __classificator_path = 'heart_class_kNN_model/k-NN.joblib'

    def __init__(self, baseline: numpy.ndarray, waves: Dict):
        '''
        Constructor of an object of heart_class.

        Args:
            baseline (numpy.ndarray): Filtered ECG signal received from module 
            "ECG_Baseline"

            waves (Dict): Dictionary containing 5 keys which are    
            characteristic points of ECG signal: P-onsets, P-ends, QRS-onsets, 
            QRS-ends, T-ends and values as preferalby ndarrays.

            fs (int): Sampling frequency of the original signal.
        '''
        self.baseline = baseline
        self.qrs_onsets = waves['QRS_onsets']
        self.qrs_ends = waves['QRS_ends']
        self.number_of_qrs = min(len(self.qrs_onsets),len(self.qrs_ends))

    def run(self):
        '''
        Runs the whole QRS detection and clustering process.

        Returns:
            A heart_class_result object.
        '''
        qrs_fragments = self.__cut_signal_into_fragments()
        qrs_features_vectors = heart_class.__fe.get_features_vectors(qrs_fragments)
        self.__load_model()
        qrs_origins = self.__predict_qrs_origins(qrs_features_vectors).tolist()

        labels_report = heart_class.__lh.report_number_of_each_label(qrs_origins)
        
        supraventricular_qrs_label = heart_class.__lh.labels_translation['Supraventricular beat']
        supraventricular_qrs_indexes = heart_class.__lh.derive_indexes_of_label(qrs_origins, supraventricular_qrs_label)
        supraventricular_qrs_vectors = self.__extract_vectors_with_indexes(qrs_features_vectors, supraventricular_qrs_indexes)
        clusters_supraventricular, clusters_num = self.__ch.perform_k_means_with_undefined_num_of_clusters(supraventricular_qrs_vectors)

        supraventricular_features_separated_by_clusters = self.__seperate_features_by_clusters(
            qrs_features_vectors, clusters_supraventricular, clusters_num
        )

        ventricular_qrs_label = heart_class.__lh.labels_translation['Ventricular beat']
        ventricular_qrs_indexes = heart_class.__lh.derive_indexes_of_label(qrs_origins, ventricular_qrs_label)
        ventricular_qrs_vectors = self.__extract_vectors_with_indexes(qrs_features_vectors, ventricular_qrs_indexes)
        clusters_ventricular, clusters_num_2 = self.__ch.perform_k_means_with_undefined_num_of_clusters(ventricular_qrs_vectors)

        ventricular_features_separated_by_clusters = self.__seperate_features_by_clusters(
            qrs_features_vectors, clusters_ventricular, clusters_num_2
        )

        return heart_class_result(
            labels_report, 
            supraventricular_features_separated_by_clusters, 
            ventricular_features_separated_by_clusters )

    def __load_model(self):
        '''
        Load sklearn model in .joblib format by path specified in __classificator_path.
        '''
        self.k_NN = load(heart_class.__classificator_path)

    def __predict_qrs_origins(self, fragments_features_vectors: List):
        '''
        Classify vectors of features by using model loaded with __load_model.
        
        Args:
            fragments_features_vectors (List): list of vectors of features

        Returns:
            (List): list of predicted labels in encoded format (0,1,2)
        '''
        return self.k_NN.predict(fragments_features_vectors)

    def __cut_signal_into_fragments(self) -> List:
        '''
        Cut continous signal into list of QRS fragments with use of values from
        class constructor: baseline and waves.

        Returns:
            (List): list of QRS fragments
        '''
        fragments = []
        for index in range(self.number_of_qrs):
            start = self.qrs_onsets[index]
            stop = self.qrs_ends[index]
            signal_fragment = self.baseline[start:stop]
            fragments.append(signal_fragment)
        return fragments

    def __seperate_features_by_clusters(self, qrs_features_vectors: List, cluster_list: List, num_of_clusters: int) -> List:
        '''
        Group vectors of features from one of classified classes (e.g. 'Ventricular beat') 
        into clusters. Method uses instance of __lh class.
        
        Args:
            qrs_features_vectors (List): list of vectors of features
            cluster_list (List): list of cluster labels (first value corresponds to 
            first item in qrs_features_vectors ecg.) 
            num_of_clusters (int): number of clusters

        Returns:
            (List): list of clustered vectors
        '''
        features_separated_by_clusters = []
        for cluster in range(num_of_clusters):
            indexes = heart_class.__lh.derive_indexes_of_label(cluster_list, cluster)
            cluster_data = self.__extract_vectors_with_indexes(qrs_features_vectors, indexes)
            features_separated_by_clusters.append(cluster_data)
        return features_separated_by_clusters

    
    def __extract_vectors_with_indexes(self, qrs_features_vectors: List, indexes: List[float]) -> List:
        '''
        Extract values from qrs_features_vectors list that are placed under desired indexes.

        Args:
            qrs_features_vectors (List): list of vectors of features
            indexes: (List[float]): list of desired indexes

        Returns:
            (numpy.ndarray): array that contains values from desired indexes
        '''
        return numpy.take(qrs_features_vectors, indexes, 0).tolist()


##### Example with ECG_BASELINE, R_PEAKS and WAVES #####
#from ecg_baseline import ecg_baseline
#from pan_tompkins import pan_tompkins
#from Waves_slope import waves_slope

#path='C:/Users/micha/Desktop/dadm projekt/rec_1'
#ecgb=ecg_baseline(path,'butter',5,15)
#signal_ecg,Fs = ecgb.load()

#path='C:/Users/micha/Desktop/dadm projekt/rec_1' #set path
#ecgb=ecg_baseline(path,'butter',5,15)
#_,Fs = ecgb.load()

#ecg=ecgb.filtering()

#peaks = pan_tompkins(ecg, Fs).fit()

#waves = waves_slope(ecg, peaks, Fs)
#waves_dict = waves.slope_waves()

#heart_class = heart_class(ecg, waves_dict)
#heart_class_result_example = heart_class.run()

'''
###### Plot results ########
import matplotlib.pyplot as plt

###Sumowanie do paczek
list_of_heart_class_result = []

summary_labels_report = dict()
for result in list_of_heart_class_result:
    summary_labels_report = dict(collections.Counter(summary_labels_report)+collections.Counter(result.get_labels_report()))
print(summary_labels_report)

#print quantities
print(heart_class_result_example.get_labels_report())

#plot 1
fig = plt.figure(1)
ax = plt.axes(projection='3d')
for i in range(3):
    x = []
    y = []
    z = []
    color = numpy.random.random(3,)
    for ind, result in enumerate(list_of_heart_class_result):
        if(ind < result.get_number_of_clusters_1()):
            for val in result.get_axis_for_index(1,"X",ind):
                x.append(val)
            for val in result.get_axis_for_index(1,"Y",ind):
                y.append(val)
            for val in result.get_axis_for_index(1,"Z",ind):
                z.append(val)
    ax.scatter(x, y, z, color=color, marker='^', label='Cluster {}'.format(i+1))
ax.set_xlabel("Feature 1")
ax.set_ylabel("Feature 2")
ax.set_zlabel("Feature 3")
plt.title("Supraventricular beats clustering")
plt.legend()

#plot2
fig = plt.figure(2)
ax = plt.axes(projection='3d')
#labels_1 = heart_class_result_example.get_labels_2()
for i in range(3):
    x = []
    y = []
    z = []
    color = numpy.random.random(3,)
    for ind, result in enumerate(list_of_heart_class_result):
        if(ind < result.get_number_of_clusters_2()):
            for val in result.get_axis_for_index(2,"X",ind):
                x.append(val)
            for val in result.get_axis_for_index(2,"Y",ind):
                y.append(val)
            for val in result.get_axis_for_index(2,"Z",ind):
                z.append(val)
    ax.scatter(x, y, z, color=color, marker='^', label='Cluster {}'.format(i+1))
ax.set_xlabel("Feature 1")
ax.set_ylabel("Feature 2")
ax.set_zlabel("Feature 3")
plt.title("Ventricular beats clustering")
plt.legend()

#display
plt.show()
'''