# -*- coding: utf-8 -*-
"""
Created on Sun Oct 31 14:32:45 2021

@author: Mejbi
"""

# UWAGA!
# Uaktualniona wersja algorytmu
# Powinna działać poprawnie z modułami ECG_BASELINE i R_PEAKS
# Dodano brakujące "self" w klasie i naprawiono błąd z dzieleniem przez 0
# Zmieniono tresholdy na bardziej adaptacyjne
# Dodano opisy klas i metod
# W razie problemów proszę o kontakt


#%%

import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from scipy import sparse
import math


class wavesSlope:
    
    def __init__(self, ecg, r_peaks, Fs): 
        
        """
        Class initialization.
        
        Args:
            ecg (one-dimensional array, float): ECG signal, filtered in the ECG_BASELINE module
            r_peaks (one-dimensional array, int): sample numbers corresponding to R waves from the R_PEAKS module
            Fs (int): sampling frequency
            
        Returns:
            class object
        """
        
        self.ecg = ecg 
        self.r_peaks = r_peaks
        self.Fs = Fs

        
    # ECG signal difference
    def signalDiff(self):
        
        """
        Determines the ECG signal difference.
        
        Args:
            none
            
        Returns:
            diff (list, float): ECG signal difference
        """
    
        diff = [self.ecg[i+1] - self.ecg[i] for i in range(self.ecg.shape[0]-1)]
        diff.append(0)
            
        return diff

        
    # QRS detection 
    def slopeQrs(self):
        
        """
        Determines the sample numbers for the Q and S waves.
        
        Args:
            none
            
        Returns:
            q_peaks (list, int): sample numbers corresponding to the Q wave
            s_peaks (list, int): sample numbers corresponding to the S wave
        """
        
        # Time between two points
        T = 1/self.Fs * 1000 #ms
        
        # Parameters necessary to determine the time windows
        QRS_half = 75    
        QS_wait = 10
        samples_QRS_half = round(QRS_half/T)
        samples_QS_wait = round(QS_wait/T)
    
        # Signal difference
        ecg_diff = self.signalDiff()
        
        # Window for determining the Q and S wave
        window_Q = np.zeros(self.ecg.shape)
        window_S = np.zeros(self.ecg.shape)
    
        for i in range(self.r_peaks.shape[0]):
            
            start_Q = 0 if self.r_peaks[i] < samples_QRS_half else self.r_peaks[i] - samples_QRS_half
            start_S = self.r_peaks[i]
            end_Q = self.r_peaks[i] + 1
            end_S = self.ecg.shape[0] if self.r_peaks[i] > (self.ecg.shape[0] - samples_QRS_half) else self.r_peaks[i] + samples_QRS_half + 1
            
            for j in range(start_Q,end_Q):
                window_Q[j] = 1
                
            for j in range(start_S,end_S):
                window_S[j] = 1
            
        # Q wave
        q_peaks = list()
    
        for i in range(self.r_peaks.shape[0]):
            
            q_temp = list()
            indx = list()
            j = self.r_peaks[i] - samples_QS_wait
            
            while window_Q[j] != 0 and j > 0:
                q_temp.append(ecg_diff[j])
                indx.append(j)
                j -= 1
    
            zero_cross = np.where(np.diff(np.sign(q_temp)))[0]
            if zero_cross.size != 0:
                q_peak = indx[zero_cross[0]]
                q_peaks.append(q_peak)
            elif j > 0:
                q_peaks.append(j)
            
        # S wave
        s_peaks = list()
    
        for i in range(self.r_peaks.shape[0]):
            
            s_temp = list()
            indx = list()
            j = self.r_peaks[i] + samples_QS_wait
            
            while window_S[j] != 0 and j < (self.ecg.shape[0]-1):
                s_temp.append(ecg_diff[j])
                indx.append(j)
                j += 1
                
            zero_cross = np.where(np.diff(np.sign(s_temp)))[0]
            if zero_cross.size != 0:
                s_peak = indx[zero_cross[0]+1]
                s_peaks.append(s_peak)
            elif j < (self.ecg.shape[0]-1):
                s_peaks.append(j)
    
        return  q_peaks, s_peaks
    
    
    # QRS_onset and QRS_end detection 
    def slopeQrsOnsetOffset(self):

        """
        Determines the sample numbers for the QRS_onset and QRS_end points.
        
        Args:
            none
            
        Returns:
            qrs_onsets (list, int): sample numbers corresponding to the QRS_onset point
            qrs_offsets (list, int): sample numbers corresponding to the QRS_end point
        """
        
        # Q and S wave
        q_peaks, s_peaks = self.slopeQrs()
        
        # Time between two points
        T = 1/self.Fs * 1000 #ms
        
        # Parameters necessary to determine the time windows
        QRS_onset_offset = 40
        QRS_wait = 10
        samples_QRS_onset_offset = round(QRS_onset_offset/T)
        samples_QRS_wait = round(QRS_wait/T)
        
        # Signal difference
        ecg_diff = self.signalDiff()
        
        # Window for determining the QRS_onset and QRS_end
        window_onset = np.zeros(self.ecg.shape)
        window_offset = np.zeros(self.ecg.shape)
        
        for i in range(len(q_peaks)):
            
            start_onset = 0 if q_peaks[i] < samples_QRS_onset_offset else q_peaks[i] - samples_QRS_onset_offset
            end_onset = q_peaks[i] + 1
            
            for j in range(start_onset,end_onset):
                window_onset[j] = 1
                    
        for i in range(len(s_peaks)):
                
            start_offset = s_peaks[i]
            end_offset = self.ecg.shape[0] if s_peaks[i] > (self.ecg.shape[0] - samples_QRS_onset_offset - 1) else s_peaks[i] + samples_QRS_onset_offset + 1
                
            for j in range(start_offset,end_offset):
                window_offset[j] = 1
                
        # QRS_onset
        qrs_onsets = list()
    
        for i in range(len(q_peaks)):
            
            qrs_onsets_temp = list()
            indx = list()
            j = q_peaks[i] - samples_QRS_wait
            
            if j > 0:
            
                while window_onset[j] != 0 and j > 0:
                    qrs_onsets_temp.append(ecg_diff[j])
                    indx.append(j)
                    j -= 1
                
                if len(qrs_onsets_temp) != 0:
                
                    qrs_onsets_diff_temp = list(map(abs, qrs_onsets_temp))
                    qrs_onsets_diff_mean = sum(qrs_onsets_diff_temp)/len(qrs_onsets_diff_temp)
                    
                    treshold = 0.1 * qrs_onsets_diff_mean    
                    
                    qrs_onsets_potential = np.where(treshold > abs(np.asarray(qrs_onsets_temp)))[0]
                    if qrs_onsets_potential.size != 0:
                        qrs_onset = indx[qrs_onsets_potential[0]]
                        qrs_onsets.append(qrs_onset)
                    elif j > 0:
                        qrs_onsets.append(j)
                        
                else:
                    qrs_onsets.append(j)
        
        # QRS_end
        qrs_offsets = list()
    
        for i in range(len(s_peaks)):

            qrs_offsets_temp = list()
            indx = list()
            j = s_peaks[i] + samples_QRS_wait
            
            if j < self.ecg.shape[0]:
                
                while window_offset[j] != 0 and j < (self.ecg.shape[0]-1):
                    qrs_offsets_temp.append(ecg_diff[j])
                    indx.append(j)
                    j += 1
                    
                if len(qrs_offsets_temp) != 0:
                
                    qrs_offsets_diff_temp = list(map(abs, qrs_offsets_temp))
                    qrs_offsets_diff_mean = sum(qrs_offsets_diff_temp)/len(qrs_offsets_diff_temp)
                    
                    treshold = 0.1 * qrs_offsets_diff_mean 
                    
                    qrs_offsets_potential = np.where(treshold > abs(np.asarray(qrs_offsets_temp)))[0]
                    if qrs_offsets_potential.size != 0:
                        qrs_offset = indx[qrs_offsets_potential[0]]
                        qrs_offsets.append(qrs_offset)
                    elif j < (self.ecg.shape[0]-1):
                        qrs_offsets.append(j)
                        
                else:
                    qrs_offsets.append(j)
                                    
        return qrs_onsets, qrs_offsets
    

    # T detection
    def slopeT(self, qrs_onsets, qrs_offsets):

        """
        Determines the sample numbers for the T wave.
        
        Args:
            qrs_onsets (list, int): sample numbers corresponding to the QRS_onset point
            qrs_offsets (list, int): sample numbers corresponding to the QRS_end point
            
        Returns:
            t_peaks (list, int): sample numbers corresponding to the T wave
            window_t (list, int): time window for the T wave
        """
        
        # Window for determining the T wave
        window_t = np.zeros(self.ecg.shape)
        
        if qrs_onsets[1] > qrs_offsets[1]:
            for i in range(len(qrs_offsets)):
                if i < len(qrs_onsets):
                    start_t = qrs_offsets[i]+1
                    end_t = qrs_offsets[i] + round((2/3)*(qrs_onsets[i]-qrs_offsets[i]))
                    
                    for j in range(start_t,end_t):
                        window_t[j] = 1
            
        if qrs_onsets[1] < qrs_offsets[1]:
            for i in range(len(qrs_offsets)):
                if i+1 < len(qrs_onsets):
                    start_t = qrs_offsets[i]+1                    
                    end_t = qrs_offsets[i] + round((2/3)*(qrs_onsets[i+1]-qrs_offsets[i]))
                    
                    for j in range(start_t,end_t):
                        window_t[j] = 1            
            
        #T wave 
        t_peaks = list()
    
        for i in range(len(qrs_offsets)):
            
            number = i if qrs_onsets[1] > qrs_offsets[1] else i+1
            
            if number < len(qrs_onsets):
            
                t_temp = list()
                indx = list()
                j = qrs_offsets[i]+1
                
                while window_t[j] != 0:
                    t_temp.append(self.ecg[j])
                    indx.append(j)
                    j += 1
                    
                t_temp = list(map(abs, t_temp))
                
                if len(t_temp) != 0:
                    max_value = max(t_temp)
        
                    t_peak = indx[t_temp.index(max_value)]
                
                    t_peaks.append(t_peak)
              
        return t_peaks, window_t
                
    
    # T_end detection 
    def slopeTOffset(self, qrs_onsets, qrs_offsets):

        """
        Determines the sample numbers for the T_end point.
        
        Args:
            qrs_onsets (list, int): sample numbers corresponding to the QRS_onset point
            qrs_offsets (list, int): sample numbers corresponding to the QRS_end point
            
        Returns:
            t_offsets (list, int): sample numbers corresponding to the QRS_end point
        """
        
        # Signal difference
        ecg_diff = self.signalDiff()
        
        # T wave
        t_peaks, window_t = self.slopeT(qrs_onsets, qrs_offsets)
        
        # Time between two points
        T = 1/self.Fs * 1000 #ms
        
        # Parameters necessary to determine the time windows
        T_wait = 10
        samples_T_wait = round(T_wait/T)
        
        t_offsets = list()
        
        for i in range(len(t_peaks)):
            
            t_diff_temp = list()
            j = qrs_offsets[i]+1
            
            while window_t[j] != 0 and j < (self.ecg.shape[0]-1):
                t_diff_temp.append(ecg_diff[j])
                j += 1
            
            if len(t_diff_temp) != 0:
                
                t_diff_temp = list(map(abs, t_diff_temp))
                t_diff_mean = sum(t_diff_temp)/len(t_diff_temp)
                
                treshold = 0.1 * t_diff_mean
                
                t_offsets_temp = list()
                indx = list()
                k = t_peaks[i] + samples_T_wait
                
                while window_t[k] != 0:
                    t_offsets_temp.append(ecg_diff[k])
                    indx.append(k)
                    k += 1
                    
                t_offsets_temp = list(map(abs, t_offsets_temp))
                t_offsets_potential = np.where(treshold > np.asarray(t_offsets_temp))[0]
                
                if t_offsets_potential.size == 0:
                    t_offsets.append(j)
                else:   
                    t_offset = indx[t_offsets_potential[0]]
                    t_offsets.append(t_offset)
                    
            else:
                t_offsets.append(t_peaks[i])
            
        return t_offsets
    
    
    # P detection
    def slopeP(self, qrs_onsets, t_offsets):

        """
        Determines the sample numbers for the P wave.
        
        Args:
            qrs_onsets (list, int): sample numbers corresponding to the QRS_onset point
            t_offsets (list, int): sample numbers corresponding to the QRS_end point
            
        Returns:
            p_peaks (list, int): sample numbers corresponding to the P wave
            window_p (list, int): time window for the P wave
        """
        
        # Window for determining the P wave
        window_p = np.zeros(self.ecg.shape)
        
        for i in range(len(t_offsets)): 
            
            start_p = t_offsets[i]+1
            
            if t_offsets[1] < qrs_onsets[1]:
                end_p = qrs_onsets[i]-1
            elif t_offsets[1] > qrs_onsets[1]:
                end_p = qrs_onsets[i+1]-1
            
            for j in range(start_p,end_p):
                    window_p[j] = 1
                    
        # P wave 
        p_peaks = list()
    
        for i in range(len(t_offsets)):
            
            p_temp = list()
            indx = list()
            j = t_offsets[i]+1
            
            while window_p[j] != 0:
                p_temp.append(self.ecg[j])
                indx.append(j)
                j += 1
                
            max_value = max(p_temp)
    
            p_peak = indx[p_temp.index(max_value)]
            
            p_peaks.append(p_peak)
              
        return p_peaks, window_p
    
    
    # P_onset and P_end detection
    def slopePOonsetOffset(self, qrs_onsets, t_offsets):

        """
        Determines the sample numbers for the P_onset and P_end points.
        
        Args:
            qrs_onsets (list, int): sample numbers corresponding to the QRS_onset point
            t_offsets (list, int): sample numbers corresponding to the QRS_end point
            
        Returns:
            p_onsets (list, int): sample numbers corresponding to the P_onset point
            p_offsets (list, int): sample numbers corresponding to the P_end point
        """
        
        # Signal difference
        ecg_diff = self.signalDiff()
        
        # P wave
        p_peaks, window_p = self.slopeP(qrs_onsets, t_offsets)
        
        # Time between two points
        T = 1/self.Fs * 1000 #ms
        
        # Parameters necessary to determine the time windows
        P_wait = 20
        samples_P_wait = round(P_wait/T)
        
        # P_onset
        p_onsets = list()
    
        for i in range(len(p_peaks)):
            
            p_onsets_temp = list()
            indx = list()
            j = p_peaks[i] - samples_P_wait
            
            while window_p[j] != 0:
                p_onsets_temp.append(ecg_diff[j])
                indx.append(j)
                j -= 1
            
            if len(p_onsets_temp) != 0:
                p_onsets_temp = list(map(abs, p_onsets_temp))
                
                p_diff_mean_onset = sum(p_onsets_temp)/len(p_onsets_temp)
                treshold_onset = 0.2 * p_diff_mean_onset
    
                p_onsets_potential = np.where(treshold_onset > np.asarray(p_onsets_temp))[0]
                
                if p_onsets_potential.size == 0:
                    p_onsets.append(j)
                else:   
                    p_onset = indx[p_onsets_potential[0]]
                    p_onsets.append(p_onset)
                    
            else:
                p_onsets.append(t_offsets[i]+1)

        # P_end
        p_offsets = list()
    
        for i in range(len(p_peaks)):
            
            p_offsets_temp = list()
            indx = list()
            j = p_peaks[i] + samples_P_wait
            
            while window_p[j] != 0:
                p_offsets_temp.append(ecg_diff[j])
                indx.append(j)
                j += 1

            if len(p_offsets_temp) != 0:
                
                p_offsets_temp = list(map(abs, p_offsets_temp))
                
                p_diff_mean_offset = sum(p_offsets_temp)/len(p_offsets_temp)
                
                treshold_offset = 0.2 * p_diff_mean_offset
                
                p_offsets_potential = np.where(treshold_offset > np.asarray(p_offsets_temp))[0]
                
                if p_offsets_potential.size == 0:
                    p_offsets.append(j)
                else:   
                    p_offset = indx[p_offsets_potential[0]]
                    p_offsets.append(p_offset)
                    
            else:
                if t_offsets[1] < qrs_onsets[1]:
                    p_offsets.append(qrs_onsets[i]-1)
                elif t_offsets[1] > qrs_onsets[1]:
                    p_offsets.append(qrs_onsets[i+1]-1)
        
        return p_onsets, p_offsets
    
    
    # All points detection 
    def slopeWaves(self):

        """
        Determines all characteristic points of the ECG signal.
        
        Args:
            none
            
        Returns:
            waves (dictionary with 5 pairs of key:value)
                P_ends (one-dimensional array, int): sample numbers corresponding to the P_end point
                P_onsets (one-dimensional array, int): sample numbers corresponding to the P_onset point
                QRS_ends (one-dimensional array, int): sample numbers corresponding to the QRS_end point
                QRS_onsets (one-dimensional array, int): sample numbers corresponding to the QRS_onset point
                T_ends (one-dimensional array, int): sample numbers corresponding to the T_end point
        """
        
        qrs_onsets, qrs_offsets = self.slopeQrsOnsetOffset()
        t_offsets = self.slopeTOffset(qrs_onsets, qrs_offsets)
        p_onsets, p_offsets = self.slopePOonsetOffset(qrs_onsets, t_offsets)
        
        qrs_onsets = np.array(qrs_onsets)
        qrs_offsets = np.array(qrs_offsets)
        t_offsets = np.array(t_offsets)
        p_onsets = np.array(p_onsets)
        p_offsets = np.array(p_offsets)

        waves = {'QRS_onsets': qrs_onsets,
                'QRS_ends': qrs_offsets,
                'T_ends': t_offsets,
                'P_onsets': p_onsets,
                'P_ends': p_offsets}
        
        return waves
    
    
# #%% Example with ECG_BASELINE and R_PEAKS
#
# path = 'F:/Studia M2/Algorytmy/103' # change path
#
# ecgb=ecg_baseline(path)
# signal,fs=ecgb.load()
#
# plt.plot(signal)
#
# #%%
#
# #butter
# ecg_butter=ecgb.filtering_butter(1,15,4)
#
# #savgol
# ecg_savgol1=ecgb.filtering_savgol(143,8)
# ecg_savgol2=ecgb.filtering_savgol(2401,3)
# ecg_savgol=ecg_savgol1-ecg_savgol2
#
# #moving_average
# ecg_ma1=ecgb.moving_average(10)
# ecg_ma2=ecgb.moving_average(100)
# ecg_ma=ecg_ma1-ecg_ma2
#
# ecg_ma=NormalizeData(ecg_ma)
# ecg_savgol=NormalizeData(ecg_savgol)
# ecg_butter=NormalizeData(ecg_butter)
#
# #%%
#
# peaks_butter = pan_tompkins(ecg_butter, fs).fit()
# waves_butter = wavesSlope(ecg_butter, peaks_butter, fs)
# waves_dict_butter = waves_butter.slopeWaves()
#
# #%%
# peaks_savgol = pan_tompkins(ecg_savgol, fs).fit()
# waves_savgol = wavesSlope(ecg_savgol, peaks_butter, fs)
# waves_dict_savgol = waves_savgol.slopeWaves()
#
# #%%
# peaks_ma = pan_tompkins(ecg_ma, fs).fit()
# waves_ma = wavesSlope(ecg_ma, peaks_ma, fs)
# waves_dict_ma = waves_ma.slopeWaves()
#
#
# #%%
#
# plt.plot(ecg_savgol)
# for xc in qrs_onsets:
#     plt.axvline(x=xc, ls=':', color='g')
# for xc in qrs_offsets:
#     plt.axvline(x=xc, ls=':', color='r')
# for xc in t_offsets:
#     plt.axvline(x=xc, ls=':', color='y')
# for xc in p_onsets:
#     plt.axvline(x=xc, ls=':', color='c')
# for xc in p_offsets:
#     plt.axvline(x=xc, ls=':', color='m')
#
# #%%
#
# qrs_onsets_values = [ecg_butter[i] for i in waves_dict_butter['QRS_onsets']]
# qrs_offsets_values = [ecg_butter[i] for i in waves_dict_butter['QRS_ends']]
# t_offsets_values = [ecg_butter[i] for i in waves_dict_butter['T_ends']]
# p_onsets_values = [ecg_butter[i] for i in waves_dict_butter['P_onsets']]
# p_offsets_values = [ecg_butter[i] for i in waves_dict_butter['P_ends']]
#
# plt.figure(1)
# plt.plot(ecg_butter, label='Sygnał EKG')
# plt.plot(waves_dict_butter['QRS_onsets'],qrs_onsets_values, 'go', label='QRS_onset')
# plt.plot(waves_dict_butter['QRS_ends'],qrs_offsets_values, 'ro', label='QRS_end')
# plt.plot(waves_dict_butter['T_ends'], t_offsets_values, 'yo', label='T_end')
# plt.plot(waves_dict_butter['P_onsets'],p_onsets_values, 'co', label='P_onset')
# plt.plot(waves_dict_butter['P_ends'],p_offsets_values, 'mo', label='P_end')
# plt.title('Punkty charakterystyczne EKG dla sygnału rzeczywistego', size=20)
# plt.xlabel('Numer próbki [-]', size=15)
# plt.ylabel('Amplituda [mV]', size=15)
# plt.legend(loc='upper right', fontsize=15)
#
# #%%
#
# qrs_onsets_values = [ecg_savgol[i] for i in waves_dict_savgol['QRS_onsets']]
# qrs_offsets_values = [ecg_savgol[i] for i in waves_dict_savgol['QRS_ends']]
# t_offsets_values = [ecg_savgol[i] for i in waves_dict_savgol['T_ends']]
# p_onsets_values = [ecg_savgol[i] for i in waves_dict_savgol['P_onsets']]
# p_offsets_values = [ecg_savgol[i] for i in waves_dict_savgol['P_ends']]
#
# plt.figure(2)
# plt.plot(ecg_savgol, label='Sygnał EKG')
# plt.plot(waves_dict_savgol['QRS_onsets'],qrs_onsets_values, 'go', label='QRS_onset')
# plt.plot(waves_dict_savgol['QRS_ends'],qrs_offsets_values, 'ro', label='QRS_end')
# plt.plot(waves_dict_savgol['T_ends'], t_offsets_values, 'yo', label='T_end')
# plt.plot(waves_dict_savgol['P_onsets'],p_onsets_values, 'co', label='P_onset')
# plt.plot(waves_dict_savgol['P_ends'],p_offsets_values, 'mo', label='P_end')
# plt.title('Punkty charakterystyczne EKG dla sygnału rzeczywistego', size=20)
# plt.xlabel('Numer próbki [-]', size=15)
# plt.ylabel('Amplituda [mV]', size=15)
# plt.legend(loc='upper right', fontsize=15)
#
# #%%
#
# qrs_onsets_values = [ecg_ma[i] for i in waves_dict_ma['QRS_onsets']]
# qrs_offsets_values = [ecg_ma[i] for i in waves_dict_ma['QRS_ends']]
# t_offsets_values = [ecg_ma[i] for i in waves_dict_ma['T_ends']]
# p_onsets_values = [ecg_ma[i] for i in waves_dict_ma['P_onsets']]
# p_offsets_values = [ecg_ma[i] for i in waves_dict_ma['P_ends']]
#
# plt.figure(3)
# plt.plot(ecg_ma, label='Sygnał EKG')
# plt.plot(waves_dict_ma['QRS_onsets'],qrs_onsets_values, 'go', label='QRS_onset')
# plt.plot(waves_dict_ma['QRS_ends'],qrs_offsets_values, 'ro', label='QRS_end')
# plt.plot(waves_dict_ma['T_ends'], t_offsets_values, 'yo', label='T_end')
# plt.plot(waves_dict_ma['P_onsets'],p_onsets_values, 'co', label='P_onset')
# plt.plot(waves_dict_ma['P_ends'],p_offsets_values, 'mo', label='P_end')
# plt.title('Punkty charakterystyczne EKG dla sygnału rzeczywistego', size=20)
# plt.xlabel('Numer próbki [-]', size=15)
# plt.ylabel('Amplituda [mV]', size=15)
# plt.legend(loc='upper right', fontsize=15)
