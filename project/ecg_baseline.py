import numpy as np
from scipy.signal import butter, filtfilt
from math import factorial

class ecgBaseline:
    
    def __init__(self, signal, fs):  
        """
        Class initialization
        
        Args:
            signal(array of float): ECG signal
            fs(int): sampling frequency
            
        Returns:
            class object

        """
        self.signal = signal 
        self.fs=fs
    
    
    def butter_bandpass (self):
        
        """
        returns coefficients of bandpass filter
            
        Returns:
            b (array of float): numerator
            a (array of float) denominator
            
        """
        sampling_freq=self.fs
        nyquist_freq = sampling_freq / 2
        normalized_low = self.low_cutoff_freq / nyquist_freq
        normalized_high = self.high_cutoff_freq / nyquist_freq
        b, a = butter (self.order, [normalized_low, normalized_high], "bandpass", analog=False)    # 'b' is the numerator, 'a' the denominator
        return b, a
    
    
    def filtering_butter(self,low_cutoff_freq,high_cutoff_freq,order):
        
        """
        filtering signal with the Butterworth filter
        
        Args:
            low_cutoff_freq (float): low cutoff frequency
            high_cutoff_freq (float): high cutoff frequency
            order (int): order of the filter
            
        Returns:
            filtered (array of float): filtered signal
            
        """
        self.low_cutoff_freq=low_cutoff_freq
        self.high_cutoff_freq=high_cutoff_freq
        self.order=order
        b,a = self.butter_bandpass()
        filtered = filtfilt(b, a, self.signal)
        return filtered
    

    def filtering_savgol(self,window_size, order, deriv=0, rate=1):
        """
        Apply a Savitzky-Golay filter to the signal
        
        Args:
            window_size(int): the length of the window. Must be an odd integer number.
            order (int): the order of the polynomial used in the filtering.
            Must be less then `window_size` - 1.
            deriv (int): the order of the derivative to compute (default = 0 means only smoothing)
        Returns:
            ys  (array of float): filtered signal (or it's n-th derivative).

        """

        
        signal=self.signal
        order_range = range(order+1)
        half_window = (window_size -1) // 2
        b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
        m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
        firstvals = signal[0] - np.abs( signal[1:half_window+1][::-1] - signal[0] )
        lastvals = signal[-1] + np.abs(signal[-half_window-1:-1][::-1] - signal[-1])
        signal = np.concatenate((firstvals, signal, lastvals))
        return np.convolve( m[::-1], signal, mode='valid')

    def moving_average(self, window):
        
        """
        Apply a Moving Average filter to the signal
        
        Args:
            window(int): size of the window
        
        Returns:
            filtered signal
        """
        signal=self.signal
        b = np.ones(window) / window
        filtfilt(b, 1, signal)
        return filtfilt(b, 1, signal)