import numpy as np
from typing import Tuple


class HRV2:

    def __init__(self, r_peaks):
        """
        Constructor of an object of HRV2 class

        Args:
            r_peaks (one-dimensional array, int): sample numbers corresponding to R waves from the R_PEAKS module
        """
        self.r_peaks = r_peaks
        self.rr_intervals = np.abs(np.diff(self.r_peaks))
        _, self.hist_bins = np.histogram(self.rr_intervals, bins=np.arange(np.min(self.rr_intervals), np.max(self.rr_intervals)+7.8125, 7.8125))

    def count_SDs(self) -> Tuple[float, float]:
        """
        Method counting SD1 and SD2 parameters, which are necessary to Poincare plot

        Args:
            None

        Returns:
            SD1 (float): number corresponding to distribution of points across the line of identity 
            SD2 (float): number corresponding to distribution of points along the line of identity
        """
        SDNN = np.std(self.rr_intervals)
        SDSD = np.std(np.diff(self.rr_intervals))
        SD1 = np.sqrt(0.5 * SDSD**2)
        SD2 = np.sqrt(2 * SDNN**2 - 0.5 * SDSD**2)
        return SD1, SD2

    def count_TINN(self) -> float:
        """
        Method counting TINN parameter

        Args:
            None

        Returns:
            TINN (float): number corresponding to the interpolation of a triangle into the RR distribution 
        """
        binsize = 7.8125
        hist, bins = np.histogram(self.rr_intervals, bins=np.arange(np.min(self.rr_intervals), np.max(self.rr_intervals)+binsize, binsize))
        max_hist = max(hist)
        max_bin_index = list(np.where(hist == max_hist))[0][0]
        M, N, error = 0, 0, 2**14
        M_bins = bins[:max_bin_index+1]
        N_bins = bins[max_bin_index+1:]

        # finding M
        for i in range(len(M_bins)-2):
            a_M, b_M, MSE_M = self._least_squares(hist[i:max_bin_index+1], M_bins[i:])
            if MSE_M < error:
                M = -(b_M/a_M)
                error = MSE_M
                
        # finding N 
        error = 2**14
        for i in range(len(N_bins)-3):
            a_N, b_N, MSE_N = self._least_squares(hist[max_bin_index:len(hist)-i-1], N_bins[:len(N_bins)-i-1])
            if MSE_N < error:
                N = -(b_N/a_N)
                error = MSE_N
            
        TINN = N - M
        return TINN

    def count_triangular_index(self) -> float:
        """
        Method counting TRI (triangular index)

        Args:
            None

        Returns:
            TRI (float): number corresponding to the ratio between the number of RR and the maximum of the RR histogram 
        """
        binsize = 7.8125
        hist, bins = np.histogram(self.rr_intervals, bins=np.arange(np.min(self.rr_intervals), np.max(self.rr_intervals)+binsize, binsize))
        TRI = len(self.rr_intervals) / float(max(hist))
        return TRI

    def _least_squares(self, hist: np.ndarray, bins: np.ndarray) -> Tuple[float, float, float]:
        """
        Method determining interpolation of the histogram slope 

        Args:
            hist (one-dimensional array, float): values of the histogram 
            bins (one-dimensional array, float): edges of histogram ranges

        Returns:
            a (float): number corresponding to a coefficient of the interpolation
            b (float): number corresponding to b coefficient of the interpolation
            MAE (float): number corresponding to mean absolute error of the least square interpolation 
        """
        bins_mean = np.mean(bins)
        hist_mean = np.mean(hist)

        num = 0
        den = 0
        for i in range(len(bins)):
            num += (bins[i] - bins_mean)*(hist[i] - hist_mean)
            den += (bins[i] - bins_mean)**2

        a = num / den
        b = hist_mean - a*bins_mean
        Y_regression = a*bins + b
        MSE = np.square(np.subtract(hist, Y_regression)).mean()
        MAE = np.abs(np.subtract(hist, Y_regression)).mean()

        return a, b, MAE


# example of usage
"""
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import numpy as np

HRV = HRV2(NN)

___________________________________________________________________________

POINCARE PLOT
___________________________________________________________________________

sd1, sd2 = HRV.count_SDs()
RR = HRV.rr_intervals

u=RR.mean()
v=RR.mean()
a=sd1
b=sd2
t_rot=-np.pi/4 #rotation angle

t = np.linspace(0, 2*np.pi, 100)
Ell = np.array([a*np.cos(t) , b*np.sin(t)])  
R_rot = np.array([[cos(t_rot) , -sin(t_rot)],[sin(t_rot) , cos(t_rot)]])  

Ell_rot = np.zeros((2,Ell.shape[1]))
for i in range(Ell.shape[1]):
    Ell_rot[:,i] = np.dot(R_rot,Ell[:,i])

plt.figure(figsize=(10,10))
ax1 = RR[:-1]
ax2 = RR[1:]   
plt.scatter(ax1, ax2)
plt.plot( u+Ell_rot[0,:] , v+Ell_rot[1,:],'r' )
plt.xlabel('RR_n [ms]')
plt.ylabel('RR_n+1 [ms]')
plt.show()

___________________________________________________________________________

HISTOGRAM
___________________________________________________________________________

TINN = HRV.count_TINN()
TRI = HRV.count_triangular_index()
RR = HRV.rr_intervals
bins = HRV.hist_bins

plt.figure(figsize=(10,8))
plt.hist(RR, bins=bins)
plt.xlabel('RR intervals [ms]')
plt.ylabel('Number of RR intervals')
"""
