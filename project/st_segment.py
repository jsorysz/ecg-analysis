from scipy.signal import find_peaks
from scipy.interpolate import interp1d
from scipy.stats import linregress
from typing import Dict, List, Tuple
import numpy


class STSegment:
    '''
    Class for analyzing ST segment of ECG. Determines start of the segment -
    known as J point and the end of it - T-onset. Calculates a  slope of the
    segment and an offset between segments line and baseline. Classifies
    episodes to 5 categories: concave, convex, upslope, downslope, horizontal.

    Attributes:
        baseline: Numpy.ndarray containing filtered ECG signal.
        j_points: Numpy.ndarray containing indexes of J points.
        t_ends: Numpy.ndarray containing indexes of T-ends.
        p_ends: Numpy.ndarray containing indexes of P-ends.
        p_onsets: Numpy.ndarray containing indexes of P-onsets.
        qrs_onsets: Numpy.ndarray containing indexes of QRS-onsets.
        t_onsets: Numpy.ndarray containing indexes of T-onsets.
        offset: Numpy.ndarray containing ends of ST segments.
        slope: Numpy.ndarray containing slopes of ST segments.
        episode: List containing names of episodes of found segments.
        shape_episode: List containing names of shapes of episodes.
        abnormalities_found: Integer of number of found abnormalities.

    Example
    >>> ST_segment_dict = STSegment(baseline, waves).run()
    '''

    def __init__(self, baseline: numpy.ndarray, waves: Dict, fs: int) -> None:
        '''
        Constructor of an object of STSegment class.

        Args:
            baseline (numpy.ndarray): Filtered ECG signal received from module
            "ECG_Baseline"

            waves (Dict): Dictionary containing 5 keys which are
            characteristic points of ECG signal: P-onsets, P-ends, QRS-onsets,
            QRS-ends, T-ends and values as preferalby ndarrays.

            fs (int): Sampling frequency of the original signal.
        '''
        self.baseline = baseline
        self.fs = fs
        self.j_points = waves["QRS_ends"]
        self.t_ends = waves["T_ends"]
        self.p_ends = waves["P_ends"]
        self.p_onsets = waves["P_onsets"]
        self.qrs_onsets = waves["QRS_onsets"]
        self.t_onsets = list()
        self.offsets = list()
        self.slopes = list()
        self.episodes = list()
        self.shape_episodes = list()
        self.abnormalities_found = 0
        self.to_skip = False
        self.to_be_deleted = []
        self._check_length()

    # Private methods
    def _check_length(self) -> None:
        '''
        Checks the position of first and last samples of J points and T ends
        and trims the points accordingly as the proper signal should start
        with J point and end with T end.

        Args:
            none

        Returns:
            none
        '''
        if(self.j_points[0] > self.t_ends[0]):
            self.j_points = numpy.delete(self.j_points, 0)
        if(self.j_points[-1] > self.t_ends[-1]):
            self.j_points = numpy.delete(self.j_points, -1)
        return

    def _find_T_onset(self, index: int) -> int:
        '''
        Finds the T onset which is the starting point of T wave.

        Args:
            index (int): Index of analyzed sampling point.

        Returns:
            sample number corresponding to T onset.
        '''
        t_peak, _ = find_peaks(
            abs(self.baseline[self.j_points[index]:self.t_ends[index]]),
            distance=self.t_ends[index] - self.j_points[index])
        t_peak += self.j_points[index]
        if t_peak.size == 0:  # checking if there is no T peak detected
            self.to_skip = True
            return
        idxs_between = list(range(self.j_points[index], t_peak[0]))
        y_diff = [self.baseline[t_peak] - self.baseline[idx]
                  for idx in idxs_between]
        x_diff = [t_peak - idx for idx in idxs_between]
        angles = numpy.arctan2(y_diff, x_diff)  # in radians
        return idxs_between[numpy.argmax(angles)]

    def _count_offset(self, index: int) -> float:
        '''
        Determines offset between isoelectric line and ST segment amplitude at
        J point.

        Args:
            index (int): Index of analyzed sampling point.

        Returns:
            offset in mV.
        '''

        if index == 0:  # skipped pr_baseline when no p_end detected before peak
            self.avg_baseline = self.baseline[int(numpy.floor(
                (self.t_ends[index] + self.p_onsets[index]) / 2))]
        else:
            pr_baseline = self.baseline[int(numpy.floor(
                (self.p_ends[index - 1] + self.qrs_onsets[index]) / 2))]
            tp_baseline = self.baseline[int(numpy.floor(
                (self.t_ends[index] + self.p_onsets[index]) / 2))]
            self.avg_baseline = (pr_baseline + tp_baseline) / 2
        return self.baseline[self.j_points[index]] - self.avg_baseline

    def _count_slope(self, index: int) -> float:
        '''
        Determines slope of ST segment starting at J point and ending at
        T onset.

        Args:
            index (int): Index of analyzed sampling point.

        Returns:
            slope in degrees.
        '''

        y_diff = self.baseline[self.t_onsets[index]] - \
            self.baseline[self.j_points[index]]
        x_diff = (self.t_onsets[index] - self.j_points[index]) / self.fs
        return numpy.rad2deg(numpy.arctan2(y_diff, x_diff))

    def _classify_episodes(self, index: int) -> Tuple[str, str]:
        '''
        Classifies episode type and its shape depending on consecutively value
        of offset and amplitude difference between interpolated line and ECG
        signal at the middle point of segment or slope of the interpolated line.

        Args:
            index (int): Index of analyzed sampling point.

        Returns:
            episode type and shape.
        '''
        if self.offsets[index] > 0.1:
            intrpl_line = interp1d((self.j_points[index],
                                    self.t_onsets[index] + 1),
                                   (self.baseline[self.j_points[index]],
                                    self.baseline[self.t_onsets[index]]))
            intrpl_amp_diff = self.baseline[int(numpy.ceil((self.j_points[index] +
                                                            self.t_onsets[index]) / 2))] - intrpl_line(
                numpy.ceil((self.j_points[index] + self.t_onsets[index]) / 2))
            if intrpl_amp_diff >= 0:
                return tuple(("Elevation", "Convex"))
            else:
                return tuple(("Elevation", "Concave"))
        elif self.offsets[index] < -0.05:
            intrpl_line = interp1d((self.j_points[index] / self.fs,
                                    self.t_onsets[index] / self.fs),
                                   (self.baseline[self.j_points[index]],
                                    self.baseline[self.t_onsets[index]]))
            intrpl_slope = (
                intrpl_line(
                    self.t_onsets[index] / self.fs) - intrpl_line(
                    self.j_points[index] / self.fs)) / (
                self.t_onsets[index] / self.fs - self.j_points[index] / self.fs)
            if intrpl_slope > 0.5:
                return tuple(("Depression", "Up-sloping"))
            elif intrpl_slope < -0.4:
                return tuple(("Depression", "Down-sloping"))
            else:
                return tuple(("Depression", "Horizontal"))
        else:
            return tuple(("Normal", "---"))

    # Public method
    def run(self) -> Dict:
        '''
        Runs the whole analyzing process.

        Returns:
            A dictionary containing 8 pairs:
                ST_starts -> numpy.ndarray filled with indexes of J points
                which are the starting points of ST segment.
                ST_ends -> numpy.ndarray filled with indexes of T-onsets which
                are the ending points of ST segment.
                offsets -> numpy.ndarray filled with float values of ends of
                corresponding segments.
                slopes -> numpy.ndarray filled with float values of slopes of
                corresponding segments.
                episodes -> numpy.ndarray filled with strings of episode names
                of corresponding segments.
                shape_episodes -> numpy.ndarray filled with string of episodes'
                shapes names of corresponding segments.
                abnormalities_found -> integer value of abnonmalities found in
                the whole signal.
                percent_abnormalities -> float value of percentage of found
                abnormalities in the whole signal.
        '''
        for index in range(len(self.j_points)):
            t_onset = self._find_T_onset(index)
            if self.to_skip:
                self.to_skip = False
                self.t_onsets.append('_')
                self.offsets.append('_')
                self.to_be_deleted.append(index)
                continue
            else:
                self.t_onsets.append(t_onset)
                self.offsets.append(self._count_offset(index))
                self.slopes.append(self._count_slope(index))
                episode, shape_episode = self._classify_episodes(index)
                self.episodes.append(episode)
                self.shape_episodes.append(shape_episode)

        self.j_points = numpy.delete(self.j_points, self.to_be_deleted)
        self.t_onsets = [i for i in self.t_onsets if i != '_']
        self.offsets = [i for i in self.offsets if i != '_']
        self.abnormalities_found = sum(
            map(lambda episode: episode != "Normal", self.episodes))

        return {
            "ST_starts": numpy.asarray(self.j_points),
            "ST_ends": numpy.asarray(self.t_onsets),
            "offsets": numpy.asarray(self.offsets),
            "slopes": numpy.asarray(self.slopes),
            "episodes": numpy.asarray(self.episodes),
            "shape_episodes": numpy.asarray(self.shape_episodes),
            "abnormalities_found": self.abnormalities_found,
            "percent_abnormalities": self.abnormalities_found /
            len(self.episodes) * 100}
