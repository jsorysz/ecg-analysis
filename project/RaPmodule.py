import queue
import time
import loaderAPI
from ecg_baseline import ecgBaseline
from pan_tompkins import pan_tompkins
from hilbert import hilbertPeaks
from Waves_slope import wavesSlope
from hrv2 import HRV2
from DFA import DFA
from heart_class import heart_class
from st_segment import STSegment
import numpy as np

def read_and_process_module(q_to_readprocess: queue.Queue, q_to_binarymanager: queue.Queue):


	pack_no = 0

	while True:
		try:
			value = q_to_readprocess.get(block=False)
		except queue.Empty:
			time.sleep(0.1)
		else:
			data_container = {
				"loaded_signal": {
					"signal": [],
					"fs": 0,
					"len": 0,
					"name": '',
					"sample_range": [],
					"redundancy": []
					},
				"ecg_baseline": [],
				"r_peaks": [], 
				"waves": { 
					"QRS_onsets": [],
					"QRS_ends": [],
					"T_ends": [],
					"P_onsets": [],
					"P_ends": []
					},
				"heart_class": object,
				"st_segment": {
					"ST_starts": np.empty(1, dtype = "int32"),
					"ST_ends": np.empty(1, dtype = "int32"),
					"offsets": np.empty(1, dtype = "float32"),
					"slopes": np.empty(1, dtype = "float32"),
					"episodes": [str],
					"shape_episodes": [str],
					"abnormalities_found": 0,
					"percent_abnormalities": 0
					}
				}

			if (value["command"] == "read_and_process"):
				out = loaderAPI.read_file(value["filepath"], value["channel"], value["sample_range"], value["redundancy"])
				data_container["loaded_signal"] = out
				time1 = time.perf_counter()

				#ECG BASELINE
				ecg_bas = ecgBaseline(data_container["loaded_signal"]["signal"], data_container["loaded_signal"]["fs"])
				if (value["f_type"] == 0):
					data_container["ecg_baseline"] = ecg_bas.filtering_butter(2,15,4)
				elif (value["f_type"] == 1):
					ecg_savgol1 = ecg_bas.filtering_savgol(85,7)
					ecg_savgol2 = ecg_bas.filtering_savgol(501,6)
					data_container["ecg_baseline"] = ecg_savgol1-ecg_savgol2
				elif (value["f_type"] == 2):
					ecg_ma1 = ecg_bas.moving_average(20)
					ecg_ma2 = ecg_bas.moving_average(50)
					data_container["ecg_baseline"] = ecg_ma1-ecg_ma2
				else:
					data_container["ecg_baseline"] = ecg_bas.filtering_butter(2,15,4)

				#CUTTING SIGNAL
				data_container["loaded_signal"], data_container["ecg_baseline"] = loaderAPI.cut_redundant_1(data_container["loaded_signal"], data_container["ecg_baseline"])

				#R PEAKS
				if (value["r_type"] == 0):
					data_container["r_peaks"] = pan_tompkins(data_container["ecg_baseline"], data_container["loaded_signal"]["fs"]).fit()
				elif (value["r_type"] == 1):
					data_container["r_peaks"] = hilbertPeaks(data_container["ecg_baseline"], data_container["loaded_signal"]["fs"]).fit()
				else:
					data_container["r_peaks"] = pan_tompkins(data_container["ecg_baseline"], data_container["loaded_signal"]["fs"]).fit()
				
				# WAVES
				waves = wavesSlope(data_container["ecg_baseline"], data_container["r_peaks"], data_container["loaded_signal"]["fs"])
				waves_dict = waves.slopeWaves()
				data_container["waves"] = waves_dict

				#ST SEGMENT
				ST_segment_dict = STSegment(data_container["ecg_baseline"], data_container["waves"], data_container["loaded_signal"]["fs"]).run()
				data_container["st_segment"] = ST_segment_dict

				#CUTTING SIGNAL
				data_container = loaderAPI.cut_redundant_2(data_container)

				#HEART CLASS
				heart_class_o = heart_class(data_container["ecg_baseline"], data_container["waves"])
				data_container["heart_class"] = heart_class_o.run()

				q_to_binarymanager.put({
                    "command": "write",
                    "data": data_container
                })

			elif (value["command"] == "exit"):
				#print("exiting read and process module")
				return
			elif (value["command"] == "new_read"):
				#print("newread")
				pack_no = 0
				q_to_binarymanager.put({
                    "command": "newfile"
                })
			elif (value["command"] == "end_of_read"):
				q_to_binarymanager.put({
                    "command": "done"
                })

