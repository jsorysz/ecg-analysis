
import numpy as np
from scipy.signal import butter, filtfilt, find_peaks
import pandas as pd


class pan_tompkins:
    
    def __init__(self, data, sample_rate):

        self.data = data
        self.sample_rate = sample_rate


    def fit(self, normalized_cut_offs=None, butter_filter_order=2, padlen=150, window_size=None):
        
        # 1.Noise cancellationusing bandpass filter
        self.filtered_BandPass = self.band_pass_filter(normalized_cut_offs, butter_filter_order, padlen)
        
        # 2.derivate filter to get slpor of the QRS
        self.derviate_pass = self.derivative_filter()

        # 3.Squaring to enhance dominant peaks in QRS
        self.square_pass = self.squaring()

        # 4.To get info about QRS complex
        self.integrated_signal = self.moving_window_integration( window_size)
        
        #Treshold
        avg=np.mean(self.integrated_signal)
        #treshold=avg*4.48
        treshold=avg
        
        # 5.Finding peaks
        self.peaks=self.finding_peaks(treshold)

        return self.peaks


    def band_pass_filter(self, normalized_cut_offs=None, butter_filter_order=2, padlen=150):
    
        # Calculate nyquist sample rate and cutoffs
        nyquist_sample_rate = self.sample_rate / 2

        # calculate cutoffs
        if normalized_cut_offs is None:
            normalized_cut_offs = [5/nyquist_sample_rate, 15/nyquist_sample_rate]
        else:
            assert type(self.sample_rate ) is list, "Cutoffs should be a list with [low, high] values"

        # butter coefficinets 
        b_coeff, a_coeff = butter(butter_filter_order, normalized_cut_offs, btype='bandpass')[:2]

        # apply forward and backward filter
        filtered_BandPass = filtfilt(b_coeff, a_coeff, self.data, padlen=padlen)
        
        return filtered_BandPass


    def derivative_filter(self):
        
        derviate_pass= np.diff(self.band_pass_filter())

        return derviate_pass


    def squaring(self):
     
        square_pass= self.derivative_filter() **2

        return square_pass 


    def moving_window_integration(self, window_size=None):

        if window_size is None:
            assert self.sample_rate is not None, "if window size is None, sampling rate should be given"
            window_size = int(0.08 * int(self.sample_rate))  # given in paper 150ms as a window size
        

        # define integrated signal
        integrated_signal = np.zeros_like(self.squaring())

        # cumulative sum of signal
        cumulative_sum = self.squaring().cumsum()

        # estimationof area/ integral below the curve deifnes the data
        integrated_signal[window_size:] = (cumulative_sum[window_size:] - cumulative_sum[:-window_size]) / window_size

        integrated_signal[:window_size] = cumulative_sum[:window_size] / np.arange(1, window_size + 1)

        return integrated_signal
    
    def finding_peaks(self,treshold):
        peaks, _ =find_peaks(self.moving_window_integration(),height=treshold,distance=200)
        peaks=peaks-13
        return peaks
    
    def inegrated(self, normalized_cut_offs=None, butter_filter_order=2, padlen=150, window_size=None):
        
        # 1.Noise cancellationusing bandpass filter
        self.filtered_BandPass = self.band_pass_filter(normalized_cut_offs, butter_filter_order, padlen)
        
        # 2.derivate filter to get slpor of the QRS
        self.derviate_pass = self.derivative_filter()

        # 3.Squaring to enhance dominant peaks in QRS
        self.square_pass = self.squaring()

        # 4.To get info about QRS complex
        self.integrated_signal = self.moving_window_integration( window_size)

        return self.integrated_signal
    
'''
ECG_data = ecg #array from ecg_baseline
sampling  =360
    
peaks = pan_tompkins(ECG_data, sampling).fit()

#import matplotlib.pyplot as plt
#plt.plot(ecg)
#plt.plot(peaks, ecg[peaks], "x")
#plt.xlim(0, 5000)
#plt.show()
'''
