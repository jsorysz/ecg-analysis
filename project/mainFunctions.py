import loaderAPI
import queue
import numpy as np

def load_commander(filepath: str, samples_per_batch: int, redundant_samples: [int,int], q_to_readprocess: queue.Queue, filter_type, r_peaks_type):
    fileProps = loaderAPI.get_file_props(filepath)
    nr_of_reads = np.floor((fileProps["samples"]-redundant_samples[0]*2-redundant_samples[1]*2) / samples_per_batch)
    nr_of_reads = int(nr_of_reads)
    samples_to_read = samples_per_batch + np.floor(((fileProps["samples"]-redundant_samples[0]*2-redundant_samples[1]*2) % samples_per_batch) / nr_of_reads)
    reads=[]
    for i in range(nr_of_reads):
        reads.append(i)

    q_to_readprocess.put({
        "command": "new_read"
        })

    for n in reads:
        samples_from_to = [int(redundant_samples[0] + redundant_samples[1] + (n*samples_to_read)), int(redundant_samples[0] + redundant_samples[1] + ((n+1)*samples_to_read))]
        q_to_readprocess.put({
            "command": "read_and_process",
            "filepath": filepath,
            "channel": 0,
            "sample_range": samples_from_to,
            "redundancy": redundant_samples,
            "f_type": filter_type,
            "r_type": r_peaks_type
            })

    q_to_readprocess.put({
        "command": "end_of_read"
        })

def close_all_modules(q_to_readprocess: queue.Queue, q_to_binarymanager: queue.Queue):
    q_to_readprocess.put({
        "command": "exit"
        })
    q_to_binarymanager.put({
        "command": "exit"
        })
