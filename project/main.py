import loaderAPI
import numpy as np
import queue
import threading
import time
import matplotlib.pyplot as plt

from RaPmodule import read_and_process_module
from BINmodule import binary_manager_module
from mainFunctions import load_commander, close_all_modules
from GUI_main import gui_start

#QUEUES
q_to_readprocess = queue.Queue()
q_to_binarymanager = queue.Queue()
q_to_main = queue.Queue()
q_to_gui = queue.Queue()

#INIT MODULES
readprocess_module = threading.Thread(target=read_and_process_module, args=(q_to_readprocess, q_to_binarymanager, ))
binarymanager_module = threading.Thread(target=binary_manager_module, args=(q_to_binarymanager, q_to_main, ))
gui_module = threading.Thread(target=gui_start, args=(q_to_gui, q_to_main, ))

#START MODULES
readprocess_module.start()
binarymanager_module.start()
gui_module.start()

#MAIN LOOP
#variable section
path = ""
bath_size = 50000
redundant_1 = 100
redundant_2 = 700

#loop section
#path = ../mit-bih-arrhythmia-database-1.0.0/103
while True:
		try:
			value = q_to_main.get(block=False)
		except queue.Empty:
			time.sleep(0.1)
		else:
			#FROM GUI
			if (value["command"] == "load_path"):
				try:
					fileprops = loaderAPI.get_file_props(value["data"]["path"])
				except:
					q_to_gui.put({
						"command": "error",
						"data": "Wrong filepath"
						})
				else:
					path = value["data"]["path"]
					q_to_gui.put({
						"command": "update_metadata",
						"data": fileprops
						})

			if (value["command"] == "start_rap"):
				if (path != ""):
					load_commander(path, bath_size, [redundant_1, redundant_2], q_to_readprocess, value["data"]["filter_type"], value["data"]["r_peaks_type"])
				else:
					q_to_gui.put({
						"command": "error",
						"data": "No filepath"
						})
			if (value["command"] == "exit"):
				close_all_modules(q_to_readprocess, q_to_binarymanager)
				break
			if (value["command"] == "request_data"):
				q_to_binarymanager.put({
					"command": "read",
					"data": value["data"]
					})

			#FROM BIN
			if (value["command"] == "pass_ecg_rr_wv_st"):
				q_to_gui.put({
					"command": "update_ecg_rr_wv_st",
					"data": value["data"]
					})
			if (value["command"] == "pass_done"):
				q_to_gui.put({
					"command": "analysis_done",
					"data": value["data"]
					})

#EXIT
readprocess_module.join()
binarymanager_module.join()
gui_module.join()
