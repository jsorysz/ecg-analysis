# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 01:30:18 2021

@author: alesn
"""

import numpy as np


class DFA:
    
    def __init__(self, r_peaks):
        self.r_peaks = r_peaks

    def RR_intervals(self):
        """
        Funkcja oblicza długosci interewałów RR
        
        Args:
            r_peaks(list): tablica próbek załamków R
        
        Returns:
            rr_intervals(list): tablica długosci interwałów RR

        """
        rr_intervals = np.diff(self.r_peaks)
        return rr_intervals
    
    
    def signal_integration(self):
        """
        Funkcja całkująca tachogram
        
        Args:
            r_intervals(list): tablica długosci interwałów RR
        
        Returns:
            integrated(list): tachogram poddany całkowaniu
            sig_len(int): ilosć próbek sygnału

        """
        rr_intervals=self.RR_intervals()
        integrated = np.cumsum(rr_intervals - np.mean(rr_intervals))
        sig_len = integrated.shape[0];
        return integrated, sig_len
    
    
    def short_time(self):
        """
        Funkcja wyznaczająca wartosci fluktuacji krótkoczasowych
        
        Args:
            windows(list): tablica długosci okien krótkoczasowych
            integrated(list): tachogram poddany całkowaniu
            sig_len(int): ilosć próbek sygnału
            num_of_win(int): ilosć okien mieszczących się w całej długosci sygnału
            samples(int): ilosć próbek poddanych analizie danym oknem
        
        Returns:
            F1(list): tablica wartosci fluktuacji dla okien krótkoczasowych

        """
        integrated, sig_len = self.signal_integration()
        windows = np.arange(4,17,1)
        F1 = np.empty(len(windows))

        for i, window in enumerate(windows):
            a = 0
            num_of_win = (int(sig_len) // int(window))
            samples = num_of_win*window
            analyzed_sig = np.arange(0,samples,1)
            y_m = np.empty(samples)
    
            for j in range(0,num_of_win):
                x = np.arange(a,a+window,1)
                y = integrated[x]
                pf = np.polyfit(x,y,1) 
                pv = np.polyval(pf, x)
                y_m[x] = pv
                a = a+window 
        
            F1[i] = np.sqrt(sum((integrated[analyzed_sig] - y_m) ** 2) / samples) 

        return F1
    
    
    def long_time(self):
        """
        Funkcja wyznaczająca wartosci fluktuacji długoczasowych
        
        Args:
            windows(list): tablica długosci okien długoczasowych
            integrated(list): tachogram poddany całkowaniu
            sig_len(int): ilosć próbek sygnału
            num_of_win(int): ilosć okien mieszczących się w całej długosci sygnału
            samples(int): ilosć próbek poddanych analizie danym oknem
        
        Returns:
            F2(list): tablica wartosci fluktuacji dla okien krótkoczasowych

        """
        integrated, sig_len = self.signal_integration()
        windows = np.arange(16,65,1)
        F2 = np.empty(len(windows))

        for i, window in enumerate(windows):
            a = 0
            num_of_win = (int(sig_len) // int(window))
            samples = num_of_win*window
            analyzed_sig = np.arange(0,samples,1)
            y_m = np.empty(samples)
    
            for j in range(0,num_of_win):
                x = np.arange(a,a+window,1)
                y = integrated[x]
                pf = np.polyfit(x,y,1) 
                pv = np.polyval(pf, x)
                y_m[x] = pv
                a = a+window 
        
            F2[i] = np.sqrt(sum((integrated[analyzed_sig] - y_m) ** 2) / samples) 

        return F2
    
    
    def out(self):
        """
        Funkcja wyznaczająca wartosci nachylenia wykresów logarytmiczno logarytmicznych fluktuacji krótko i długoczasowych
        
        Args:
            x1(list): tablica długosci okien krótkoczasowych
            x2(list): tablica długosci okien długoczasowych
            F1(list): tablica fluktuacji krótkoczasowcyh
            F2(list): tablica fluktuacji długoczasowych
        
        Returns:
            x(list): tablica długosći analizowanych okien
            F(list): tablica fluktuacji krótko i długoczasowych
            pv1(list): tablica punktów prostej regresji dla fluktuacji krótkoczasowych
            pv2(list): tablica punktów prostej regresji dla fluktuacji długooczasowych
            alpha(list): tablica współczynników nachylenia prostej regresjii (współczynniki alpha1 i alpha2)
        """
        F1=self.short_time()
        F2=self.long_time()
        x1 = np.arange(4,17,1)
        x2 = np.arange(16,65,1)
        x = np.concatenate((x1,x2),axis=0)
        F = np.concatenate((F1,F2),axis=0)
        
        alpha = np.empty(2)
        
        pf1 = np.polyfit(np.log10(x[0:13]),np.log10(F[0:13]),1) 
        pv1 = np.polyval(pf1, np.log10(x[0:13]))
        alpha[0] = str(round(pf1[0], 3))
        
        pf2 = np.polyfit(np.log10(x[13:62]),np.log10(F[13:62]),1) 
        pv2 = np.polyval(pf2, np.log10(x[13:62]))
        alpha[1] = str(round(pf2[0], 3))
        
        return x, F, pv1, pv2, alpha
    

"""
import matplotlib.pyplot as plt

test=DFA(r_peaks)
dfa=test.out()

print(dfa2[4])                                              #wyświetlenie współczynników alfa1 i alfa2
plt.plot(np.log10(dfa[0]),np.log10(dfa[1]), 'b.')           #wykres logarytmiczno logarytmiczny fluktuacji do rozmiaru okna
plt.plot(np.log10(dfa[0][0:13]), dfa[2], color='green')     #wykres prostej regresji dla fluktuacji krótkoczasowych
plt.plot(np.log10(dfa[0][13:62]), dfa[3], color='red')      #wykres prostej regresji dla fluktuacji długoczasowych
plt.show()
"""