import queue
import time
from numpy import array
import numpy as np
import os
from hrv2 import HRV2
from DFA import DFA
from HRV1 import FrequencyParameters, TimeParameters
import datetime
import struct

def binary_manager_module(q_to_binarymanager: queue.Queue, q_to_main: queue.Queue):
    
    pack_no = 0
    state_flag = "initializing"
    r_peaks = np.empty(1, dtype = "int32")
    waves = {
        "QRS_onsets": np.zeros(1, dtype = "int32"),
        "QRS_ends": np.zeros(1, dtype = "int32"),
        "T_ends": np.zeros(1, dtype = "int32"),
        "P_onsets": np.zeros(1, dtype = "int32"),
        "P_ends": np.zeros(1, dtype = "int32")
        }
    st_segment = {
		"ST_starts": np.empty(1, dtype = "int32"),
		"ST_ends": np.empty(1, dtype = "int32"),
		"offsets": np.empty(1, dtype = "float32"),
		"slopes": np.empty(1, dtype = "float32"),
		"episodes": np.empty(1, dtype = "str"),
		"shape_episodes": np.empty(1, dtype = "str"),
		"abnormalities_found": 0,
		"percent_abnormalities": 0
		}
    heart_class = []

    while True:
        try:
            value = q_to_binarymanager.get(block=False)
        except queue.Empty:
            time.sleep(0.1)
        else:
            if (value["command"] == "write"):
                time1 = time.perf_counter()
                
                file = open("temp_file.bin", "ab")
                data = array(value["data"]["ecg_baseline"], "float32")
                data.tofile(file)
                file.close()

                #RAM SAVING
                to_add = len(value["data"]["loaded_signal"]["signal"]) * pack_no
                
                #rpeaks
                for i in range(len(value["data"]["r_peaks"])):
                    value["data"]["r_peaks"][i] = value["data"]["r_peaks"][i] + to_add

                r_peaks = np.concatenate([r_peaks,value["data"]["r_peaks"]], dtype = "int32", casting = 'unsafe')
                if (pack_no == 0):
                    r_peaks = np.delete(r_peaks, [0])

                #waves
                for i in range(len(value["data"]["waves"]["QRS_onsets"])):
                    value["data"]["waves"]["QRS_onsets"][i] = value["data"]["waves"]["QRS_onsets"][i] + to_add

                for i in range(len(value["data"]["waves"]["QRS_ends"])):
                    value["data"]["waves"]["QRS_ends"][i] = value["data"]["waves"]["QRS_ends"][i] + to_add

                for i in range(len(value["data"]["waves"]["T_ends"])):
                    value["data"]["waves"]["T_ends"][i] = value["data"]["waves"]["T_ends"][i] + to_add

                for i in range(len(value["data"]["waves"]["P_ends"])):
                    value["data"]["waves"]["P_ends"][i] = value["data"]["waves"]["P_ends"][i] + to_add

                for i in range(len(value["data"]["waves"]["P_onsets"])):
                    value["data"]["waves"]["P_onsets"][i] = value["data"]["waves"]["P_onsets"][i] + to_add

                waves["QRS_onsets"] = np.concatenate([waves["QRS_onsets"],value["data"]["waves"]["QRS_onsets"]], dtype = "int32", casting = 'unsafe')
                if (pack_no == 0):
                    waves["QRS_onsets"] = np.delete(waves["QRS_onsets"], [0])
                waves["QRS_ends"] = np.concatenate([waves["QRS_ends"],value["data"]["waves"]["QRS_ends"]], dtype = "int32", casting = 'unsafe')
                if (pack_no == 0):
                    waves["QRS_ends"] = np.delete(waves["QRS_ends"], [0])
                waves["T_ends"] = np.concatenate([waves["T_ends"],value["data"]["waves"]["T_ends"]], dtype = "int32", casting = 'unsafe')
                if (pack_no == 0):
                    waves["T_ends"] = np.delete(waves["T_ends"], [0])
                waves["P_ends"] = np.concatenate([waves["P_ends"],value["data"]["waves"]["P_ends"]], dtype = "int32", casting = 'unsafe')
                if (pack_no == 0):
                    waves["P_ends"] = np.delete(waves["P_ends"], [0])
                waves["P_onsets"] = np.concatenate([waves["P_onsets"],value["data"]["waves"]["P_onsets"]], dtype = "int32", casting = 'unsafe')
                if (pack_no == 0):
                    waves["P_onsets"] = np.delete(waves["P_onsets"], [0])

                #st segment
                for i in range(len(value["data"]["st_segment"]["ST_starts"])):
                    value["data"]["st_segment"]["ST_starts"][i] = value["data"]["st_segment"]["ST_starts"][i] + to_add

                for i in range(len(value["data"]["st_segment"]["ST_ends"])):
                    value["data"]["st_segment"]["ST_ends"][i] = value["data"]["st_segment"]["ST_ends"][i] + to_add

                for i in range(len(value["data"]["st_segment"]["offsets"])):
                    value["data"]["st_segment"]["offsets"][i] = value["data"]["st_segment"]["offsets"][i] + to_add

                for i in range(len(value["data"]["st_segment"]["slopes"])):
                    value["data"]["st_segment"]["slopes"][i] = value["data"]["st_segment"]["slopes"][i] + to_add

                st_segment["ST_starts"] = np.concatenate([st_segment["ST_starts"],value["data"]["st_segment"]["ST_starts"]], dtype = "int32", casting = 'unsafe')
                if (pack_no == 0):
                    st_segment["ST_starts"] = np.delete(st_segment["ST_starts"], [0])

                st_segment["ST_ends"] = np.concatenate([st_segment["ST_ends"],value["data"]["st_segment"]["ST_ends"]], dtype = "int32", casting = 'unsafe')
                if (pack_no == 0):
                    st_segment["ST_ends"] = np.delete(st_segment["ST_ends"], [0])

                st_segment["offsets"] = np.concatenate([st_segment["offsets"],value["data"]["st_segment"]["offsets"]], dtype = "float32", casting = 'unsafe')
                if (pack_no == 0):
                    st_segment["offsets"] = np.delete(st_segment["offsets"], [0])

                st_segment["slopes"] = np.concatenate([st_segment["slopes"],value["data"]["st_segment"]["slopes"]], dtype = "float32", casting = 'unsafe')
                if (pack_no == 0):
                    st_segment["slopes"] = np.delete(st_segment["slopes"], [0])

                st_segment["episodes"] = np.concatenate([st_segment["episodes"],value["data"]["st_segment"]["episodes"]], dtype = "str", casting = 'unsafe')
                if (pack_no == 0):
                    st_segment["episodes"] = np.delete(st_segment["episodes"], [0])

                st_segment["shape_episodes"] = np.concatenate([st_segment["shape_episodes"],value["data"]["st_segment"]["shape_episodes"]], dtype = "str", casting = 'unsafe')
                if (pack_no == 0):
                    st_segment["shape_episodes"] = np.delete(st_segment["shape_episodes"], [0])

                st_segment["abnormalities_found"] += value["data"]["st_segment"]["abnormalities_found"]
                part = pack_no/(pack_no+1)
                st_segment["percent_abnormalities"] = st_segment["percent_abnormalities"] * part  + value["data"]["st_segment"]["percent_abnormalities"] * (1-part)

                #heart class
                heart_class.append(value["data"]["heart_class"])

                pack_no = pack_no + 1

            elif (value["command"] == "read"):
                if(state_flag == "done"):
                    file_r = open("temp_file.bin", "rb")
                    file_r.seek(value["data"]["offset"]*4, 0)
                    read_value = np.zeros(1, dtype = "float32")
                    for i in range(value["data"]["num_of_samples"]):
                        read_value = np.concatenate([read_value, struct.unpack("f", file_r.read(4))])
                    file_r.close()
                    read_value = np.delete(read_value, [0])
                    
                    st_strt = st_segment["ST_starts"][(st_segment["ST_starts"] >= value["data"]["offset"]) & (st_segment["ST_starts"] < (value["data"]["offset"] + value["data"]["num_of_samples"]))]
                    st_en = st_segment["ST_ends"][(st_segment["ST_ends"] >= value["data"]["offset"]) & (st_segment["ST_ends"] < (value["data"]["offset"] + value["data"]["num_of_samples"]))]
                    st_off = st_segment["offsets"][(st_segment["ST_starts"] >= value["data"]["offset"]) & (st_segment["ST_starts"] < (value["data"]["offset"] + value["data"]["num_of_samples"]))]
                    st_slo = st_segment["slopes"][(st_segment["ST_starts"] >= value["data"]["offset"]) & (st_segment["ST_starts"] < (value["data"]["offset"] + value["data"]["num_of_samples"]))]
                    st_ep = st_segment["episodes"][(st_segment["ST_starts"] >= value["data"]["offset"]) & (st_segment["ST_starts"] < (value["data"]["offset"] + value["data"]["num_of_samples"]))]
                    st_sh_ep =st_segment["shape_episodes"][(st_segment["ST_starts"] >= value["data"]["offset"]) & (st_segment["ST_starts"] < (value["data"]["offset"] + value["data"]["num_of_samples"]))]
                    
                    if (st_strt[0]>st_en[0]):
                        st_en = np.delete(st_en, [0])

                    q_to_main.put({
                        "command": "pass_ecg_rr_wv_st",
                        "data":{
                            "ecg": read_value,
                            "r_peaks": r_peaks[(r_peaks >= value["data"]["offset"]) & (r_peaks < (value["data"]["offset"] + value["data"]["num_of_samples"]))],
                            "waves": {
                                "QRS_onsets": waves["QRS_onsets"][(waves["QRS_onsets"] >= value["data"]["offset"]) & (waves["QRS_onsets"] < (value["data"]["offset"] + value["data"]["num_of_samples"]))],
                                "QRS_ends": waves["QRS_ends"][(waves["QRS_ends"] >= value["data"]["offset"]) & (waves["QRS_ends"] < (value["data"]["offset"] + value["data"]["num_of_samples"]))],
                                "T_ends": waves["T_ends"][(waves["T_ends"] >= value["data"]["offset"]) & (waves["T_ends"] < (value["data"]["offset"] + value["data"]["num_of_samples"]))],
                                "P_onsets": waves["P_onsets"][(waves["P_onsets"] >= value["data"]["offset"]) & (waves["P_onsets"] < (value["data"]["offset"] + value["data"]["num_of_samples"]))],
                                "P_ends": waves["P_ends"][(waves["P_ends"] >= value["data"]["offset"]) & (waves["P_ends"] < (value["data"]["offset"] + value["data"]["num_of_samples"]))]
                                },
                            "st_segment":{
                                "ST_starts": st_strt,
                                "ST_ends": st_en,
                                "offsets": st_off,
                                "slopes": st_slo,
                                "episodes": st_ep,
                                "shape_episodes": st_sh_ep
                                }

                            }
                        })

                else:
                    pass

            elif (value["command"] == "newfile"):
                if os.path.exists("temp_file.bin"):
                    os.remove("temp_file.bin")
                    pack_no = 0

                #RESETING VALUES
                r_peaks = np.empty(1, dtype = "int32")
                waves = {
                    "QRS_onsets": np.empty(1, dtype = "int32"),
                    "QRS_ends": np.empty(1, dtype = "int32"),
                    "T_ends": np.empty(1, dtype = "int32"),
                    "P_onsets": np.empty(1, dtype = "int32"),
                    "P_ends": np.empty(1, dtype = "int32")
                    }
                st_segment = {
		            "ST_starts": np.empty(1, dtype = "int32"),
		            "ST_ends": np.empty(1, dtype = "int32"),
		            "offsets": np.empty(1, dtype = "float32"),
		            "slopes": np.empty(1, dtype = "float32"),
		            "episodes": np.empty(1, dtype = "str"),
		            "shape_episodes": np.empty(1, dtype = "str"),
		            "abnormalities_found": 0,
		            "percent_abnormalities": 0
		            }
                heart_class = []

                state_flag = "writing"
                #print("opening new file")

            elif (value["command"] == "end_write"):
                state_flag = "idle"
                #print("closing file")

            elif (value["command"] == "done"):
                #HRV2
                hrv2 = HRV2(r_peaks)
                SD1, SD2 = hrv2.count_SDs()
                RR = hrv2.rr_intervals
                TRI = hrv2.count_triangular_index()
                TINN = hrv2.count_TINN()
                bins = hrv2.hist_bins

                t_rot=-np.pi/4 
                t = np.linspace(0, 2*np.pi, 100)
                Ell = np.array([SD1*np.cos(t) , SD2*np.sin(t)])  
                R_rot = np.array([[np.cos(t_rot) , -np.sin(t_rot)],[np.sin(t_rot) , np.cos(t_rot)]])  

                Ell_rot = np.zeros((2,Ell.shape[1]))
                for i in range(Ell.shape[1]):
                    Ell_rot[:,i] = np.dot(R_rot,Ell[:,i])

                Ell_rot[0,:] = Ell_rot[0,:] + RR.mean()
                Ell_rot[1,:] = Ell_rot[1,:] + RR.mean()

                #HRF DFA
                test=DFA(r_peaks)
                dfa=test.out()

                #HRV1
                Tim = TimeParameters(r_peaks)
                Freq = FrequencyParameters(r_peaks, method = 'welch', sampling_rate = 10)
                Total_Power, ULF, VLF, LF, HF, LF_HF_ratio = Freq.get_frequency_parameters()
                periodogram = Freq.get_periodogram_data()

                #SENDING TO MAIN
                file_size = os.path.getsize("temp_file.bin")

                q_to_main.put({
                    "command": "pass_done",
                    "data":{
                        "num_of_samples": file_size / 4 - 1,
                        "hrv1": {
                            "MeanRR":Tim.Mean_RR(),
                            "SDNN": Tim.SDNN(),
                            "SDANN":Tim.SDANN(),
                            "RMSSD":Tim.RMSSD(),
                            "SDNNindex":Tim.SDANNindex(),
                            "SDSD":Tim.SDSD(),
                            "NN50":Tim.NN50(),
                            "pNN50":Tim.pNN50(),
                            "total_power": Total_Power,
                            "ulf": ULF,
                            "vlf": VLF,
                            "lf": LF,
                            "hf": HF,
                            "lf_hf_ratio": LF_HF_ratio,
                            "periodogram": periodogram
                            },
                        "hrv2": {
                            "sd1": SD1,
                            "sd2": SD2,
                            "rr": RR,
                            "tri": TRI,
                            "tinn": TINN,
                            "ell_rot": Ell_rot,
                            "bins": bins
                            },
                        "hrv_dfa": dfa,
                        "heart_class": heart_class
                        }
                    })

                #REPORT FILE
                report_str = "report_"+datetime.datetime.now().strftime("%d/%m/%Y_%H:%M:%S")+"\n"
                report_str = report_str + "ST segment analysis:\n"
                report_str = report_str + "abnormalities found: " + str(st_segment["abnormalities_found"]) + "\n"
                report_str = report_str + "percent abnormalities: " + str(st_segment["percent_abnormalities"]) + "%" + "\n"
                report_str = report_str + "HRV analysis:\n"
                report_str = report_str + "ULF: " + str(ULF) + ", VLF: " + str(VLF) + ", LF: " + str(LF) + ", HF: " + str(HF) + ", LF to HF ratio: " + str(LF_HF_ratio)+ "\n"
                report_str = report_str + "SD1: " + str(SD1) + ", SD2: " + str(SD2) + ", Triangular index: " + str(TRI) + ", TINN index: " + str(TINN) + "\n"
                report_str = report_str + "alpha1: " + str(dfa[4][0]) + ", alpha2: " + str(dfa[4][1]) + "\n"
                file_report = open("report_"+datetime.datetime.now().strftime("%d_%m_%Y_%H_%M_%S")+".txt", "w")
                file_report.write(report_str)
                file_report.close()

                state_flag = "done"

            elif (value["command"] == "exit"): 
                print("exiting binary manager module")
                return
