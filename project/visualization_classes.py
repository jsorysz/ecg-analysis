import numpy as np
from datetime import datetime, timedelta
import pyqtgraph as pg
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from pyqtgraph.Qt import QtCore, QtGui

class DateAxisItem(pg.AxisItem):
    '''
    pytgraph.AxisItem child class 
    Class used for creating X axis of ECG plot.
    The axis modifyied using tickStrings and attach_to_plot_item methods
    represents time elapsed from the beginning of the examination
    in format: hh:mm:ss:ms
    '''
    # Max width in pixels reserved for each label in axis
    _pxLabelWidth = 80

    def __init__(self, *args, **kwargs):
        pg.AxisItem.__init__(self, *args, **kwargs)
        self._oldAxis = None

    def tickStrings(self, values, scale, spacing):
        '''
        Convert vector values which contains time  
        in ms to desired format (hh:mm:ss:ms)
        '''
        result_t = []
        if not values:
            return []

        for x in values:
            try:
                if(x>=0):
                    t = datetime.fromtimestamp(x) - timedelta(hours=1)
                    result_t.append(t.strftime("%H:%M:%S:%f")[:-4])
                else:
                    t = datetime.fromtimestamp(abs(x))
                    result_t.append(t.strftime("-%H:%M:%S:%f")[:-4])
            except ValueError:  
                result_t.append('')

        return result_t

    def attach_to_plot_item(self, plot_item):
        """
        Add the axis to the given PlotItem
        """
        self.setParentItem(plot_item)
        view_box = plot_item.getViewBox()
        self.linkToView(view_box)
        self._old_axis = plot_item.axes[self.orientation]['item']
        self._old_axis.hide()
        plot_item.axes[self.orientation]['item'] = self
        pos = plot_item.axes[self.orientation]['pos']
        plot_item.layout.addItem(self, *pos)
        self.setZValue(-1000)

class ECGPlot(pg.PlotWidget):
    '''
    pyqtgrapgh.PlotWidget child class
    A widget that can contain ECG plot.
    
    '''
    def __init__(self):
        pg.PlotWidget.__init__(self)
        self.setBackground('w')
        axis = DateAxisItem(orientation='bottom')
        axis.attach_to_plot_item(self.getPlotItem())
        self.setLabel('left', 'Voltage [mV]')
        self.setLabel('bottom', 'Time [hh:mm:ss:ms]')
        self.showGrid(x=True, y=True)
        self.module_result = []
        self.ecg_plotted = False
                
    def plot_ECG(self, signal, fs=360, start=0):        
        """
        Plot ECG signal in the ECGPlotwidget.

        Parameters
        ----------
        signal (numpy.array): ECG signal to be visualized.
        
        fs (int): sampling frequency of ECG signal
        
        start (int): the sample number at which the selected portion of 
            the ECG signal begins

        Returns
        -------
        ecg_plot_item (pyqtgraph.PlotItem): created ecg plot item

        """
        if(self.ecg_plotted == True):
             self.clear_ECG_plot()
        
        dt = 1/fs
        t = np.arange(start*dt,(start+len(signal)-0.5)*dt, dt)

        self.setLimits(xMin=t[0],xMax=t[-1])
        self.legend = self.addLegend()
        self.data = [t,signal]
        self.start = start
        self.ecg_plotted = True
        self.ecg_plot_item = self.plot(t, signal, pen='b', name='ecg baseline')
        print(type(self.ecg_plot_item))
        return self.ecg_plot_item
    
    def plot_R_peaks(self, r_peaks):
        """
        Add R peaks to the existing ECG plot.

        Parameters
        ----------
        r_peaks (list): result from the R_PEAKS module. A list containing 
            sample numbers of R peaks in the analyzed signal.
 
        Returns
        -------
        None.
        """
        self.clear_module_result()
        try:
            self.module_result.append(self.plot(self.data[0][r_peaks-self.start], 
                                                self.data[1][r_peaks-self.start], 
                                                pen=None, symbolPen = 'r', symbol='t1', name = 'R peaks'))
        except: 
            pass
        
    def plot_waves(self, waves):        
        """
        Add characteristic points to the existing ECG plot.

        Parameters
        ----------
        waves (dictionary): result from the WAVES module. A dictionary containing 
            information about characteristic points in the analyzed signal including 
            their type and location.
 
        Returns
        -------
        None.
        """
        
        self.clear_module_result()
        
        symbols = ['x', 'x', 'x', 'x', 'x']
        pens = ['r', 'g', 'm','b','y']
        try: 
            for i,wave in enumerate(waves):
                self.module_result.append(self.plot(self.data[0][waves[wave]-self.start], 
                                                    self.data[1][waves[wave]-self.start],pen=None, 
                                                    symbolPen = pens[i], symbol=symbols[i], name = wave))
        except: 
            pass
        
    def plot_ST(self, ST_segments):
        """
        Add ST segments to the existing ECG plot.

        Parameters
        ----------
        ST_segments (dictionary): result from the ST_SEGMENT module. 
            A dictionary containing information about ST segments in the analyzed
            signal including their location.
 
        Returns
        -------
        None.

        """
        self.clear_module_result()
        try:
            st_start = ST_segments['ST_starts'][0]-self.start
            st_end = ST_segments['ST_ends'][0]-self.start
            self.module_result.append(self.plot(self.data[0][st_start:st_end],
                                                self.data[1][st_start:st_end], 
                                                pen = 'r', name = 'ST segments'))
            for i in range(1,len(ST_segments['ST_starts'])):
                st_start = ST_segments['ST_starts'][i]-self.start
                st_end = ST_segments['ST_ends'][i]-self.start
                self.plot(self.data[0][st_start:st_end],
                          self.data[1][st_start:st_end], pen = 'r')      
        except:
            pass
        
    def clear_module_result(self):
        """
        Remove characteristic points or segments from the ECG plot.

        Returns
        -------
        None.
        """
        
        for i in range(len(self.module_result)):
            self.legend.removeItem(self.module_result[i])
            self.module_result[i].clear()
    
        self.module_result = []
        
    def clear_ECG_plot(self):
        """
        Clear all the data in the ECG plot item.

        Returns
        -------
        None.
        """
        self.clear_module_result()
        self.legend.removeItem(self.ecg_plot_item)
        self.ecg_plot_item.clear()
        self.ecg_plotted = False
        
        
class ECGPlotWidget(QtGui.QWidget):
    """
    QtGui.QWidget child class
    A widget that contains ECG Plot, sliders for scrolling and zooming 
    and a label for displaying coordinates of the cursor.
    """
    def __init__(self):
        super().__init__()
        self.layout = QtGui.QVBoxLayout()
        self.scroll_slider = QtGui.QScrollBar(QtCore.Qt.Horizontal)
        self.zoom_slider = QtGui.QScrollBar(QtCore.Qt.Horizontal)
        
        self.setLayout(self.layout)
        self.layout.addWidget(self.scroll_slider)
        self.layout.addWidget(self.zoom_slider)
        
        self.scroll_slider.setEnabled(False)
        self.zoom_slider.setEnabled(False)
        
        self.scroll_slider.valueChanged.connect(self.update_scroll_slider)
        self.zoom_slider.valueChanged.connect(self.update_zoom_slider)
        
        self.label = QLabel()
        self.layout.addWidget(self.label)
        
        self.plot_widget = ECGPlot()
        self.plot_widget.setMouseEnabled(x=False, y=False)
        self.layout.addWidget(self.plot_widget)
        
    def add_ECG_plot(self, ecg, fs=360, start=0):
        """
        Create plot_widget with given ECG data and adding it 
        to the window.

        Parameters:
            ecg (numpy.array): ECG signal to visualize
        
            fs (int): sampling frequency of ECG signal
        
            start (int): the sample number at which the selected portion of 
            the ECG signal begins

        Returns:
            none

        """
        self.start = start
        self.ecg_fs = fs
        #print(self.ecg_fs)
        self.ecg_dt = 1/self.ecg_fs
        
        t = np.arange(self.start*self.ecg_dt,(self.start+len(ecg))*self.ecg_dt, self.ecg_dt)
        self.data = [t,ecg]

        self.plot_item = self.plot_widget.plot_ECG(ecg, fs, self.start)
        self.scroll_slider.setEnabled(True)
        self.zoom_slider.setEnabled(True)
        self.scroll_slider.setValue(500)
        self.zoom_slider.setValue(1000)
                
        scroll_slider_val = self.scroll_slider.value()
        zoom_slider_val = self.zoom_slider.value()
        
        self.zoom_slider.setMaximum(2*(len(self.data[0])-int(scroll_slider_val))-1)
        
        self.scroll_slider.setMaximum(len(self.data[0])-int(zoom_slider_val/2)-1)
        self.scroll_slider.setMinimum(int(zoom_slider_val/2))
        self.scroll_slider.setSingleStep(0.1*zoom_slider_val/self.ecg_dt)
        
        x_max = self.data[0][scroll_slider_val + int(zoom_slider_val/2)] 
        x_min = self.data[0][scroll_slider_val - int(zoom_slider_val/2)]

        self.plot_widget.setXRange(x_min, x_max, 0)
        self.plot_widget.setYRange(min(self.data[1]), max(self.data[1]), 0)

        self.plot_item.scene().sigMouseMoved.connect(self.mouse_moved)

        
    def mouse_moved(self, point):
        """
        Actualize cursor coordinates displayed in the label.
        
        Parameters:
            point (PyQt5.QtCore.QPointF): contains information about cursor 
            location in layout coordinates

        Returns:
            none
        """
        p = self.plot_widget.plotItem.vb.mapSceneToView(point)
        if(p.x() >= 0):
            t = datetime.fromtimestamp(p.x()) - timedelta(hours=1)
        else:
            t = t = datetime.fromtimestamp(0) - timedelta(hours=1)
        self.label.setText("Time: {} \nVoltage: {} mV".format(t.strftime("%H:%M:%S:%f")[:-4], round(p.y(),3)))
            
    def update_scroll_slider(self):
        """
        Update visible X range in the plot widget according
        to the value set on scroll_slider.        

        Parameters
            none
            
        Returns
            none

        """
        scroll_slider_val = self.scroll_slider.value()
        zoom_slider_val = self.zoom_slider.value()
        
        try:
            x_max = self.data[0][scroll_slider_val + int(zoom_slider_val/2)] 
        except:
             x_max = self.data[0][-1]
             
        x_min = self.data[0][scroll_slider_val - int(zoom_slider_val/2)]
        
        self.plot_widget.setXRange(x_min, x_max, 0)
        self.zoom_slider.setMaximum(2*(len(self.data[0])-int(scroll_slider_val))-1)
        
    def update_zoom_slider(self):
        """
        Update visible X range in the plot widget according
        to the value set on zoom_slider.        

        Parameters
            none
            
        Returns
            none

        """
        scroll_slider_val = self.scroll_slider.value()
        zoom_slider_val = self.zoom_slider.value()
        
        try:
            x_max = self.data[0][scroll_slider_val + int(zoom_slider_val/2)] 
        except:
             x_max = self.data[0][-1]
             
        x_min = self.data[0][scroll_slider_val - int(zoom_slider_val/2)]
        
        self.plot_widget.setXRange(x_min, x_max, 0)
        
        self.scroll_slider.setMaximum(len(self.data[0])-int(zoom_slider_val/2)-1)
        self.scroll_slider.setMinimum(int(zoom_slider_val/2))
        self.scroll_slider.setSingleStep(0.1*zoom_slider_val/self.ecg_dt)
