# -*- coding: utf-8 -*-
"""

@author: Aleksandra Stafiej

Moduł: HRV

Time and frequency analysis of the ECG signal

"""
import numpy as np
import math
import matplotlib.pyplot as plt

from scipy import interpolate, signal
from astropy.timeseries import LombScargle

#########################################################################################################
# Class to calculate parameters for time analysis
#########################################################################################################
class TimeParameters:
    '''
        Class created to determine the timing parameters of the ECG signal
    
     Input data:
         input_data: data generated on the output of the R peaks module
    
     Class methods are functions that calculate successive time parameters of a signal
    '''
    
    def __init__(self, input_data):
        self.tachogram = input_data
        self.N = len(input_data)
        
        '''
        Time Vector
        '''
        data_times_RR = []
        for idx in range(self.N):
            if idx == 0:
                data_times_RR.append(input_data[0])
            else: 
                data_times_RR.append( input_data[idx] - input_data[idx - 1])
        
        self.data = np.array(data_times_RR)
        
    def Mean_RR(self):
        '''
        Function calculating average value of RR intervals
        
         Returns:
             average of RR intervals as a floating point number
        '''
        self.mean_RR = np.mean(self.data)
        mean_RR=self.mean_RR
        return mean_RR
    
    def SDNN(self):
        '''
            Function calculating the standard deviation of the RR intervals
        
         Returns:
             the value of the standard deviation of the RR intervals as a 
             floating point number
        '''
        SDNN_Temp = 0
        for idx in range(self.N):
            SDNN_Temp = SDNN_Temp + pow((self.mean_RR - self.data[idx]), 2)
        return math.sqrt(SDNN_Temp/(self.N - 1))
    
    def RMSSD(self):
        '''
            Function calculating the square root of the mean of the squares of 
            the differences between two consecutive RR intervals
        
         Returns:
             value of the square root from the mean of the squares of the 
             differences between two consecutive RR intervals as a floating point number
        '''
        RMSSD_Temp = 0
        for idx in range(self.N - 1):
            RMSSD_Temp = RMSSD_Temp + pow((self.data[idx+1] - self.data[idx]), 2)
        return math.sqrt(RMSSD_Temp/(self.N - 1))
    
    def NN50(self):
        '''
            A function that calculates the number of RR intervals that differ 
            by more than 50 ms
        
         Returns:
             the number of RR intervals that differ by more than 50 ms as an integer 
        '''
        Fi = 0
        for idx in range(self.N - 1):
            if (self.data[idx+1] - self.data[idx]) > 50:
                Fi += 1
        self.NN50 = Fi
        return Fi
    
    def pNN50(self):
        '''
            Function that calculates the percentage of difference between 
            RR intervals that exceed 50 ms as a percentage
        
         Returns:
             percentage of difference between RR intervals that exceed 50 ms 
             as a floating point number
        '''
        return ((self.NN50 / (self.N - 1)) * 100)
        
    def SDANN(self):
        '''
            Function calculating the standard deviation of all mean RR intervals 
            over 5-minute segments of the entire recording
        
         Returns:
             the value of the standard deviation from all average RR intervals 
             in 5-minute segments of the entire record as a floating point number
        '''
        means = []
        sums = 0
        last_idx = 0
        for idx in range(1,self.N):
            sums = sums + self.data[idx]
            if sums >= 5000:
                means.append(sums/(idx - last_idx))
                sums = 0
                last_idx = idx
        return np.std(means)
    
    def SDANNindex(self):
        '''
            Function calculating the average of the standard deviations of the RR 
        intervals over the 5-minute time segments of the entire recording
        
         Returns:
             the average value of the standard deviations of RR intervals in 
             5-minute segments of the entire record as a floating-point number
        '''
        STDs = []
        sums = 0
        last_idx = 0
        for idx in range(self.N):
            sums = sums + self.data[idx]
            if sums >= 5000:
                if (idx != 0):
                    STDs.append(np.std(self.data[last_idx:idx]))
                sums = 0
                last_idx = idx
        return np.mean(STDs)
    
    def SDSD(self):
        '''
            A function that calculates the standard deviation of the difference 
        between two adjacent RR intervals
        
         Returns:
             the value of the standard deviation of the difference between two 
             adjacent RR intervals as a floating point number
        '''
        SDSD_temp = []
        for idx in range(self.N - 1):
            SDSD_temp.append(self.tachogram[idx+1] - self.tachogram[idx])
        return np.std(SDSD_temp)
    

#########################################################################################################
# Class to calculate parameters for frequency analysis
#########################################################################################################

class FrequencyParameters:
    '''
        Class created to perform the frequency analysis of the ECG signal
    
     Input data:
         Input Data: data generated on the output of the R peaks module
    
         method: Select one method of the frequency analysis 'welch' or 'lomb'
               
         Sampling Rate: The sampling rate of the signal
               
     Attributes:
         data: The constructor function performs calculations to determine the 
         successive RR interval times
        
     Methods:
         The next methods are used to analyze the frequency, calculate the 
         frequency parameters of the signal and determine the tachogram in the 
         frequency domain with superimposed frequency ranges
         
    This class can handle signals with different signal length: less than 5 minutes, 
    less than 24 hours and more than 24 hours
    
    '''
    def __init__(self, input_data, method = 'welch', sampling_rate = 4):
        self.tachogram = input_data
        self.method = method
        self.N = len(input_data)
        self.sampling_rate = sampling_rate
        self.time_flag = 'None'
        
        # Warning if sampling frequency is too small
        if sampling_rate < 4:
            print("WARNING! \nSampling Frequency too litle! \nIt won't end up well!")
         
        # Checking The length of the data and creating handlers for different data length
        time_5_min = 1000 * 60 * 5
        time_24_h = 1000 * 60 * 60 * 24
        
        print(time_24_h)
        print(input_data[self.N-1])
        
        if input_data[self.N-1] < time_5_min:
            self.time_flag = "Less5min"
            print("WARNING! \nThe data length is too small! \nThe calculations will be garbage!")
            
        elif input_data[self.N-1] < time_24_h:
            self.time_flag = "Less24h"
            print("Less than 24h")
            
        else:
            self.time_flag = "Long"
            print("Wooo that's long")
            
        '''
        Time vector
        '''
        
        data_times_RR = []
        for idx in range(self.N):
            if idx == 0:
                data_times_RR.append(input_data[0])
            else: 
                data_times_RR.append( input_data[idx] - input_data[idx - 1])
        
        self.intervals_RR = data_times_RR

    def frequency_analysis_welch(self):
        '''
            A function that performs the frequency analysis of a signal by 
            Welch method
     
         Returns:
             frequency_PSD: The frequency corresponding to the successive 
                               elements of the PSD vector
             PSD: Power Spectral Density values
        '''
        time_vector_list = (self.tachogram - self.tachogram[0])/ 1000
        
        # ---------- Interpolacja sygnału ---------- #
        funct = interpolate.interp1d(x = time_vector_list, y = self.intervals_RR, kind = 'cubic')
        
        # Reshaping 
        reshape_time_vector = np.arange(0, time_vector_list[-1], 1 / float(self.sampling_rate))
        
        interpolated_IRR = funct(reshape_time_vector)
        interpolated_normalized_IRR = interpolated_IRR - np.mean(interpolated_IRR)

        #  --------- Compute Power Spectral Density  --------- #
        if self.time_flag == "Less5min":
            frequency_PSD, PSD = signal.welch(x = interpolated_normalized_IRR, fs = self.sampling_rate, window ='hann', nfft = 4096, nperseg = 300)
        else:
            frequency_PSD, PSD = signal.welch(x = interpolated_normalized_IRR, fs = self.sampling_rate, window ='hann', nfft = 4096, nperseg = 4096)
        
        return frequency_PSD, PSD 
        
        
    def frequency_analysis_lomb(self):
        '''
            A function that performs the frequency analysis of a signal by Lomb-Scargle method
     
         Returns:
             frequency_PSD: The frequency corresponding to the successive elements of the PSD vector
             PSD: Power Spectral Density values
        '''
        time_vector_list =  (self.tachogram - self.tachogram[0]) / 1000
        
        frequency_PSD, PSD = LombScargle(time_vector_list, self.intervals_RR, normalization='psd').autopower(minimum_frequency=0.000001, maximum_frequency=0.4)
        return frequency_PSD, PSD 
    
    
    def get_frequency_parameters(self):
        '''
            A function that calculates the frequency parameters of a signal

         Returns
         TP: total spectrum power
         ULF: spectral power in the ultra low frequency range
         VLF: Spectral power in the very low frequency range
         LF: low frequency spectrum power
         HF: Spectral power in the high frequency range
         LF_HF_ratio: low to high frequency spectral power ratio frequency
         
         Calculations take into consideration different lengths of the signal:
             if signal is smaller than 24 hours - parameter ULF will be nan!
             if signal is smaller than 25 minutes - parameters ULF and VLF 
             will be nan!

        '''
        if self.method == 'welch':
            frequency_PSD, PSD = self.frequency_analysis_welch()
            
        elif self.method == 'lomb':
            frequency_PSD, PSD = self.frequency_analysis_lomb()

        ULF_idx = frequency_PSD < 0.003
        VLF_idx = np.logical_and(frequency_PSD >= 0.003, frequency_PSD < 0.04)
        LF_idx = np.logical_and(frequency_PSD >= 0.04, frequency_PSD < 0.15)
        HF_idx = np.logical_and(frequency_PSD >= 0.15, frequency_PSD < 0.4)
    
        # Integrate using the composite trapezoidal rule
        if self.time_flag == "Less5min":
            ULF = float('nan')
            VLF = float('nan')
            LF = np.trapz(y=PSD[LF_idx], x=frequency_PSD[LF_idx])
            HF = np.trapz(y=PSD[HF_idx], x=frequency_PSD[HF_idx])
            TP = LF + HF
            
        elif  self.time_flag == "Less24h":
            ULF = float('nan')
            VLF = np.trapz(y=PSD[VLF_idx], x=frequency_PSD[VLF_idx])
            LF = np.trapz(y=PSD[LF_idx], x=frequency_PSD[LF_idx])
            HF = np.trapz(y=PSD[HF_idx], x=frequency_PSD[HF_idx])
            TP =  VLF + LF + HF
            
        else:
            ULF = np.trapz(y=PSD[ULF_idx], x=frequency_PSD[ULF_idx])
            VLF = np.trapz(y=PSD[VLF_idx], x=frequency_PSD[VLF_idx])
            LF = np.trapz(y=PSD[LF_idx], x=frequency_PSD[LF_idx])
            HF = np.trapz(y=PSD[HF_idx], x=frequency_PSD[HF_idx])
            TP = ULF + VLF + LF + HF
        
        LF_HF_ratio = LF/HF
            
        return TP, ULF, VLF, LF, HF, LF_HF_ratio
    
    def get_periodogram_data(self):
        '''
            A function aimed at returning the data needed to draw a chart
         periodogram
        
         Returns:
             periodogram: Dictionary of arrays for PSD values and responders
             the frequencies for each frequency range:
             'X_ULF' - frequencies for the ULF range (x values on the chart)
             'Y_ULF' - PSD values for the ULF range (y values on the chart)
            
             'X_VLF' - frequencies for the VLF range (x values on the chart)
             'Y_VLF' - PSD values for the VLF range (y values on the chart)
            
             'X_LF' - frequencies for the LF range (x values on the chart)
             'Y_LF' - PSD values for the LF range (y values in the chart)
            
             'X_HF' - frequencies for the HF range (x values on the chart)
             'Y_HF' - PSD values for the HF range (y values on the chart)
             
             Calculations take into consideration different lengths of the signal:
             if signal is smaller than 24 hours - arrays for ULF will only 
             contain nan!
             if signal is smaller than 25 minutes - arrays for ULF and VLF 
             will only contain nan!

        '''
        periodogram = {}
        if self.method == 'welch':
            frequency_PSD, PSD = self.frequency_analysis_welch()
            
        elif self.method == 'lomb':
            frequency_PSD, PSD = self.frequency_analysis_lomb()
            
        ULF_idx = frequency_PSD < 0.003
        VLF_idx = np.logical_and(frequency_PSD >= 0.003, frequency_PSD < 0.04)
        LF_idx = np.logical_and(frequency_PSD >= 0.04, frequency_PSD < 0.15)
        HF_idx = np.logical_and(frequency_PSD >= 0.15, frequency_PSD < 0.4)
            
        if self.time_flag == "Less5min":
            periodogram['X_ULF'] = np.array([float('nan')])
            periodogram['Y_ULF'] = np.array([float('nan')])
        
            periodogram['X_VLF'] = np.array([float('nan')])
            periodogram['Y_VLF'] = np.array([float('nan')])
            
        elif self.time_flag == "Less24h":
            periodogram['X_ULF'] = np.array([float('nan')])
            periodogram['Y_ULF'] = np.array([float('nan')])
            
            periodogram['X_VLF'] = np.array(frequency_PSD[VLF_idx])
            periodogram['Y_VLF'] = np.array(PSD[VLF_idx]/1000)
            
        else:
            periodogram['X_ULF'] = np.array(frequency_PSD[ULF_idx])
            periodogram['Y_ULF'] = np.array(PSD[ULF_idx]/1000)
        
            periodogram['X_VLF'] = np.array(frequency_PSD[VLF_idx])
            periodogram['Y_VLF'] = np.array(PSD[VLF_idx]/1000)
        
        periodogram['X_LF'] = np.array(frequency_PSD[LF_idx])
        periodogram['Y_LF'] = np.array(PSD[LF_idx]/1000)
        
        periodogram['X_HF'] = np.array(frequency_PSD[HF_idx])
        periodogram['Y_HF'] = np.array(PSD[HF_idx]/1000)

        return periodogram
    
    def get_frequency_plot(self):
        '''
        Function that draws a tachogram in the frequency space
        '''
        if self.method == 'welch':
            frequency_PSD, PSD = self.frequency_analysis_welch()
            
        elif self.method == 'lomb':
            frequency_PSD, PSD = self.frequency_analysis_lomb()
         
        plt.figure(figsize=(12, 8))
        plt.title("periodogram", fontsize=20)
        plt.xlabel("Frequency (Hz)", fontsize=15)
        plt.ylabel("PSD (s2/ Hz)", fontsize=15)   
        
        if self.time_flag == "Less5min":
            LF_idx = np.logical_and(frequency_PSD >= 0.04, frequency_PSD < 0.15)
            HF_idx = np.logical_and(frequency_PSD >= 0.15, frequency_PSD < 0.4)

            plt.fill_between(frequency_PSD[LF_idx], 0, PSD[LF_idx] / 1000, label='LF')
            plt.fill_between(frequency_PSD[HF_idx], 0, PSD[HF_idx] / 1000, label='HF')
            
        elif self.time_flag == "Less24h":
            VLF_idx = np.logical_and(frequency_PSD >= 0.003, frequency_PSD < 0.04)
            LF_idx = np.logical_and(frequency_PSD >= 0.04, frequency_PSD < 0.15)
            HF_idx = np.logical_and(frequency_PSD >= 0.15, frequency_PSD < 0.4)
            
            plt.fill_between(frequency_PSD[VLF_idx], 0, PSD[VLF_idx] / 1000, label='VLF')
            plt.fill_between(frequency_PSD[LF_idx], 0, PSD[LF_idx] / 1000, label='LF')
            plt.fill_between(frequency_PSD[HF_idx], 0, PSD[HF_idx] / 1000, label='HF')
            
        else: 
            ULF_idx =  np.logical_and(frequency_PSD >= 0.00001, frequency_PSD < 0.003)
            VLF_idx = np.logical_and(frequency_PSD >= 0.003, frequency_PSD < 0.04)
            LF_idx = np.logical_and(frequency_PSD >= 0.04, frequency_PSD < 0.15)
            HF_idx = np.logical_and(frequency_PSD >= 0.15, frequency_PSD < 0.4)
            
            plt.fill_between(frequency_PSD[ULF_idx], 0, PSD[ULF_idx] / 1000, label='ULF')
            plt.fill_between(frequency_PSD[VLF_idx], 0, PSD[VLF_idx] / 1000, label='VLF')
            plt.fill_between(frequency_PSD[LF_idx], 0, PSD[LF_idx] / 1000, label='LF')
            plt.fill_between(frequency_PSD[HF_idx], 0, PSD[HF_idx] / 1000, label='HF')

'''
#########################################################################################################
# Przykład zastosowania:
#########################################################################################################


from pan_tompkins import pan_tompkins
from ecg_baseline import ecg_baseline

# Creating input data
path='C:/Users/olcik/Desktop/Semestr9/DADM/Projekt/Algorytm/MitBihDatabase/105'
ecgb=ecg_baseline(path,'butter',5,15)
ecg=ecgb.filtering()

ECG_data = ecg #array from ecg_baseline
sampling  = 360
    
peaks = pan_tompkins(ECG_data, sampling).fit()

# Creating class for time parameters
Time = TimeParameters(peaks)

# Example for computing time parameters
print("Mean RR: ", Time.Mean_RR())

# Creating class for frequency paameters
Czestotl = FrequencyParameters(peaks, method = 'welch', sampling_rate = 10)

# Calculating Frequency parameters
Total_Power, ULF, VLF, LF, HF, LF_HF_ratio = Czestotl.get_frequency_parameters()
print("\nFrequency Parameters: \n")
print("Total Power: ", Total_Power, '\nULF: ', ULF, '\nVLF: ', VLF, '\nLF: ', LF, '\nHF: ', HF, '\nLF/HF Ratio: ', LF_HF_ratio)

# Extracting data and plot periodogram
periodogram = Czestotl.get_periodogram_data()

X1 = periodogram['X_ULF']
Y1 = periodogram['Y_ULF']
        
X2 = periodogram['X_VLF'] 
Y2 = periodogram['Y_VLF']
        
X3 = periodogram['X_LF'] 
Y3 = periodogram['Y_LF'] 
        
X4 = periodogram['X_HF'] 
Y4 = periodogram['Y_HF'] 

if math.isnan(X1[0]) and math.isnan(X2[0]):
    plt.fill_between(X3, Y3, label='LF')
    plt.fill_between(X4, Y4, label='HF')
elif math.isnan(X1[0]):
    plt.fill_between(X2, Y2, label='VLF')
    plt.fill_between(X3, Y3, label='LF')
    plt.fill_between(X4, Y4, label='HF')  
else:
    plt.fill_between(X1, Y1, label='ULF')
    plt.fill_between(X2, Y2, label='VLF')
    plt.fill_between(X3, Y3, label='LF')
    plt.fill_between(X4, Y4, label='HF')
 
# Drawing Example periodogram
# Czestotl.get_frequency_plot()
'''
