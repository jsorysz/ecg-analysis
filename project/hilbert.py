
import numpy as np
from scipy.signal import find_peaks
from scipy.signal import hilbert

class hilbertPeaks:
    
    def __init__(self, data, sample_rate):
        self.data = data
        self.sample_rate = sample_rate
    
    def fit(self):
        """
        Function carries out a R peaks detection algorithm based on Hilbert transform. 

        Returns:
            peaks (1D ndarray, int): Numbers of samples corresponding to detected R peaks.
        """
        # 1. Differentiation
        self.derivative_pass = self.derivative_filter()
        
        # 2. Hilbert Transform
        self.amplitude_envelope = self.hilbert_transform()
        
        # 3. Adaptive Tresholding
        self.first_peaks, self.final_R = self.final_peaks()
        
        return self.final_R
        
    def hilbert_transform(self):
        """
        Function carries out hilbert transform.

        Returns:
            amplitude_envelope (1D ndarray): The amplitude envelope of a hilbert transform.
        """
        self.analytic_signal = hilbert(self.derivative_filter())
        self.amplitude_envelope = np.abs(self.analytic_signal)
        
        return self.amplitude_envelope
    
    def derivative_filter(self):
        """
        Function carries out signal differentiation.

        Returns:
            derviate_pass (1D ndarray, float): differentiated signal.
        """
        
        derviate_pass= np.diff(self.data)

        return derviate_pass 
       
    def final_peaks(self):
        """
        Function carries out adaptive tresholding in order to detect R peaks.

        Returns:
            final_R_locs (1D array, int): sample numbers corrresponding to detected peaks.
        """
        
            
        ECG_env=self.amplitude_envelope
        ECG_bp=self.data
        sampling=self.sample_rate
            
        #Fiducial point
        peaks, _ =find_peaks(ECG_env,distance=200) 
            
        RRAVERAGE1 = []
        RRAVERAGE2 = []
        IWF_signal_peaks = []
        IWF_noise_peaks = []
        noise_peaks = []
        ECG_bp_peaks = np.array([])
        ECG_bp_signal_peaks = []
        ECG_bp_noise_peaks = []
        final_R_locs = []
        T_wave_found = 0  
        ECG=ECG_bp
        neigh=0.5 #size of a window used for searching for peaks
        neg=False #variable to check if an ECG has positive or negative R peaks
            
        #Learning phase 1
        initializeTime = 2 * sampling #Two seconds of initializing time during which the algorithm doesn't detect peaks
        if(len(ECG_env)==0):
            SPKI=ECG_env
        else:
            extr = max(ECG_env[:initializeTime])
            SPKI = extr * 0.25
    
        NPKI = np.mean(ECG_env[:initializeTime]) * 0.5 
        THRESHOLDI1 = NPKI + 0.25 * (SPKI-NPKI)
        THRESHOLDI2 = 0.5 * THRESHOLDI1 
            
        if(len(ECG_bp)==0):
            SPKF=ECG_bp
        else:
            if  np.where(ECG_bp==min(ECG_bp[:initializeTime]))[0][0] >= np.where(ECG_bp==max(ECG_bp[:initializeTime]))[0][0]:
                extr = max(ECG_bp[:initializeTime])
                neg=False
            else:
                    if np.mean(ECG_bp[:initializeTime])-abs(min(ECG_bp[:initializeTime])) >= abs(max(ECG_bp[:initializeTime]))-np.mean(ECG_bp[:initializeTime]):
                        extr=min(ECG_bp[:initializeTime])
                        neg=True
            SPKF = extr * 0.25 
                
            
        NPKF = np.mean(ECG_bp[:initializeTime]) * 0.5 
        THRESHOLDF1 = NPKF + 0.25 * (SPKF-NPKF)
        THRESHOLDF2 = 0.5 * THRESHOLDF1
            
        peaks = peaks[peaks > initializeTime] #test
            

        try:
            for c,peak in enumerate(peaks):      
                #Searching for the corresponding peaks in filtered signal using window +- neigh value              
                searchInterval = int(np.round(neigh * sampling))
                searchIndices = np.arange(peak - searchInterval, peak + searchInterval + 1, 1)        
                if searchIndices[0] >= 0 and all(searchIndices <= len(ECG_bp)):
                    if(len(ECG_bp[searchIndices])!=0): 
                        extr = max(ECG_bp[searchIndices]) if neg==False else min(ECG_bp[searchIndices])              
                        ECG_bp_peaks = np.append(ECG_bp_peaks, np.where(ECG_bp == extr)[0][0])          
                else:
                    if(len(ECG_bp[searchIndices[0]:len(ECG_bp)-1])!=0):
                        extr = max(ECG_bp[searchIndices[0]:len(ECG_bp)-1]) if neg==False else min(ECG_bp[searchIndices[0]:len(ECG_bp)-1])
                        ECG_bp_peaks = np.append(ECG_bp_peaks, np.where(ECG_bp == extr))
                
                if c > 0 and c < len(ECG_bp_peaks):                     
                    if c < 8:                
                        RRAVERAGE1_vec = np.diff(peaks[:c + 1]) / sampling
                        RRAVERAGE1_mean = np.mean(RRAVERAGE1_vec)
                        RRAVERAGE1.append(RRAVERAGE1_mean) 
                            
                        RR_LOW_LIMIT = 0.92 * RRAVERAGE1_mean
                        RR_HIGH_LIMIT = 1.16 * RRAVERAGE1_mean
                        RR_MISSED_LIMIT = 1.66 * RRAVERAGE1_mean                
                    else:                
                        RRAVERAGE1_vec = np.diff(peaks[c - 8:c + 1]) / sampling
                        RRAVERAGE1_mean = np.mean(RRAVERAGE1_vec)
                        RRAVERAGE1.append(RRAVERAGE1_mean) 
                
                        for rr in np.arange(0, len(RRAVERAGE1_vec)):
                            if RRAVERAGE1_vec[rr] > RR_LOW_LIMIT and RRAVERAGE1_vec[rr] < RR_HIGH_LIMIT:                              
                                RRAVERAGE2.append(RRAVERAGE1_vec[rr])                                     
                                if len(RRAVERAGE2) > 8:
                                    del RRAVERAGE2[:len(RRAVERAGE2) - 8]
                
                        if len(RRAVERAGE2) == 8:
                            RR_LOW_LIMIT = 0.92 * np.mean(RRAVERAGE2)        
                            RR_HIGH_LIMIT = 1.16 * np.mean(RRAVERAGE2)
                            RR_MISSED_LIMIT = 1.66 * np.mean(RRAVERAGE2)
                                
                                
                    #If an irregular heartbeat is detected the tresholds are lowered
                    current_RR_movavg = RRAVERAGE1[-1] 
                    if current_RR_movavg < RR_LOW_LIMIT or current_RR_movavg > RR_MISSED_LIMIT: 
                                
                        THRESHOLDI1 = 0.5 * THRESHOLDI1
                        THRESHOLDI2 = 0.5 * THRESHOLDI1
                            
                        THRESHOLDF1 = 0.5 * THRESHOLDF1
                        THRESHOLDF2 = 0.5 * THRESHOLDF1
                        
                    #Re-searching the segment with a lowered threshold if the distance between consecutive R-waves exceeds the specified limit
                    currentRRint = RRAVERAGE1_vec[-1]
                    if currentRRint > RR_MISSED_LIMIT:  
                        SBinterval = int(np.round(currentRRint * sampling))                        
                        SBdata_IWF = ECG_env[peak - SBinterval + 1:peak + 1]               
                            
                        SBdata_IWF_filtered = np.where((SBdata_IWF > THRESHOLDI1))[0]
                        if(len(SBdata_IWF[SBdata_IWF_filtered])!=0):
                            extr = max(SBdata_IWF[SBdata_IWF_filtered]) if neg==False else min(SBdata_IWF[SBdata_IWF_filtered])
                            SBdata_max_loc = np.where(SBdata_IWF == extr)[0][0]

                        if len(SBdata_IWF_filtered) > 0:   
                            SB_IWF_loc = peak - SBinterval + 1 + SBdata_max_loc
                            IWF_signal_peaks.append(SB_IWF_loc) 
                                
                            SPKI = 0.25 * ECG_env[SB_IWF_loc] + 0.75 * SPKI                         
                            THRESHOLDI1 = NPKI + 0.25 * (SPKI - NPKI)
                            THRESHOLDI2 = 0.5 * THRESHOLDI1               
                                
                            if SB_IWF_loc < len(ECG_bp):
                                SBdata_ECGfilt = ECG_bp[SB_IWF_loc - round(neigh * sampling): SB_IWF_loc]                    
                                SBdata_ECGfilt_filtered = np.where((SBdata_ECGfilt > THRESHOLDF1))[0]
                                if(len(SBdata_ECGfilt[SBdata_ECGfilt_filtered])!=0):
                                    extr = max(SBdata_ECGfilt[SBdata_ECGfilt_filtered]) if neg==False else min(SBdata_ECGfilt[SBdata_ECGfilt_filtered])
                                    SBdata_max_loc2 = np.where(SBdata_ECGfilt == extr)[0][0]
                                                
                            else:
                                SBdata_ECGfilt = ECG_bp[SB_IWF_loc - round(neigh * sampling):]
                                SBdata_ECGfilt_filtered = np.where((SBdata_ECGfilt > THRESHOLDF1))[0]
                                if(len(SBdata_ECGfilt[SBdata_ECGfilt_filtered])!=0):
                                    extr = max(SBdata_ECGfilt[SBdata_ECGfilt_filtered]) if  neg==False else min(SBdata_ECGfilt[SBdata_ECGfilt_filtered])
                                    SBdata_max_loc2 = np.where(SBdata_ECGfilt == extr)[0][0]

                                        
                            if(len(SBdata_ECGfilt[SBdata_ECGfilt_filtered])!=0):
                                if ECG_bp[SB_IWF_loc - round(neigh * sampling) + SBdata_max_loc2] > THRESHOLDF2: #Wykrycie zespołu QRS
                                                                    
                                    SPKF = 0.25 * ECG_bp[SB_IWF_loc - round(neigh * sampling) + SBdata_max_loc2] + 0.75 * SPKF                            
                                    THRESHOLDF1 = NPKF + 0.25 * (SPKF - NPKF)
                                    THRESHOLDF2 = 0.5 * THRESHOLDF1                            
                                    ECG_bp_signal_peaks.append(SB_IWF_loc - round(neigh * sampling) + SBdata_max_loc2)                                                 
                
                    #Differentiating QRS complex from a T wave    
                    if ECG_env[peak] >= THRESHOLDI1: 
                        if currentRRint > 0.20 and currentRRint < 0.36 and c > 0: 
                                
                            if(len(np.diff(ECG_env[peak - round(sampling * 0.075):peak + 1]))!=0):
                                extr = max(np.diff(ECG_env[peak - round(sampling * 0.075):peak + 1])) if  neg==False else min(np.diff(ECG_env[peak - round(sampling * 0.075):peak + 1]))         
                                maxSlope_current = extr
                                
                            if(len(np.diff(ECG_env[peaks[c - 1] - round(sampling * 0.075): peaks[c - 1] + 1]))!=0):
                                extr = max(np.diff(ECG_env[peaks[c - 1] - round(sampling * 0.075): peaks[c - 1] + 1])) if  neg==False else min(np.diff(ECG_env[peaks[c - 1] - round(sampling * 0.075): peaks[c - 1] + 1]))         
                                maxSlope_past = extr
                                if maxSlope_current < 0.5 * maxSlope_past: #T-wave found                        
                                    T_wave_found = 1                
                                        
                                    IWF_noise_peaks.append(peak)                
                                        
                                    NPKI = 0.125 * ECG_env[peak] + 0.875 * NPKI                                            
                                        
                        if not T_wave_found:                    
                            IWF_signal_peaks.append(peak)
                            
                            SPKI = 0.125 * ECG_env[peak]  + 0.875 * SPKI
                                            
                            if ECG_bp_peaks[c] > THRESHOLDF1:                                            
                                SPKF = 0.125 * ECG_bp[c] + 0.875 * SPKF 
                                ECG_bp_signal_peaks.append(ECG_bp_peaks[c])                             
                            else:
                                ECG_bp_noise_peaks.append(ECG_bp_peaks[c])
                                NPKF = 0.125 * ECG_bp[c] + 0.875 * NPKF                   
                                                    
                    elif ECG_env[peak] > THRESHOLDI1 and ECG_env[peak] < THRESHOLDI2:
                        
                        NPKI = 0.125 * ECG_env[peak]  + 0.875 * NPKI  
                        NPKF = 0.125 * ECG_bp[c] + 0.875 * NPKF
                                
                    elif ECG_env[peak] < THRESHOLDI1:
                
                        noise_peaks.append(peak)
                        NPKI = 0.125 * ECG_env[peak]  + 0.875 * NPKI            
                        ECG_bp_noise_peaks.append(ECG_bp_peaks[c])                       
                        NPKF = 0.125 * ECG_bp[c] + 0.875 * NPKF
                else:
                    if ECG_env[peak] >= THRESHOLDI1: 
                        IWF_signal_peaks.append(peak) 
                        
                        SPKI = 0.125 * ECG_env[peak]  + 0.875 * SPKI
                        if ECG_bp_peaks[c] > THRESHOLDF1:                                            
                            SPKF = 0.125 * ECG_bp[c] + 0.875 * SPKF 
                            ECG_bp_signal_peaks.append(ECG_bp_peaks[c])                             
                        else:
                            ECG_bp_noise_peaks.append(ECG_bp_peaks[c])
                            NPKF = 0.125 * ECG_bp[c] + 0.875 * NPKF                                    
                            
                    elif ECG_env[peak] > THRESHOLDI2 and ECG_env[peak] < THRESHOLDI1:
        
                        NPKI = 0.125 * ECG_env[peak]  + 0.875 * NPKI  
                        NPKF = 0.125 * ECG[c] + 0.875 * NPKF
                                                
                    elif ECG_env[peak] < THRESHOLDI2:
                        
                        noise_peaks.append(peak)
                        NPKI = 0.125 * ECG_env[peak]  + 0.875 * NPKI            
                        ECG_bp_noise_peaks.append(ECG_bp_peaks[c])                       
                        NPKF = 0.125 * ECG_bp[c] + 0.875 * NPKF       
                        
                T_wave_found = 0                
                        
                THRESHOLDI1 = NPKI + 0.25 * (SPKI - NPKI)
                THRESHOLDI2 = 0.5 * THRESHOLDI1 
                        
                THRESHOLDF1 = NPKF + 0.25 * (SPKF - NPKF)
                THRESHOLDF2 = 0.5 * THRESHOLDF1
        
            ECG_R_locs = [int(i) for i in ECG_bp_signal_peaks]
            ECG_R_locs = np.unique(ECG_R_locs)
                
            #Searching for peaks in the input signal in order to increase accuracy  
            for i in ECG_R_locs:
                ECG = np.array(ECG)
                searchInterval = int(np.round(0.02 * sampling))
                searchIndices = np.arange(i - searchInterval, i + searchInterval + 1, 1)
                searchIndices = [i.item() for i in searchIndices] 
                if(len(ECG[searchIndices])!=0):
                    extr = max(ECG[searchIndices]) if  neg==False else min(ECG[searchIndices])         
                    final_R_locs.append(np.where(ECG[searchIndices] == extr)[0][0] + searchIndices[0])
        except IndexError:
            final_R_locs = np.array(final_R_locs)    
        final_R_locs = np.array(final_R_locs)        
        return peaks, final_R_locs
    
##Example of use

# ECG_data = ecg_butter[0:6720] #subarray from array from ecg_baseline
# fs  =360

# peaks2 = hilbertPeaks(ECG_data, fs).fit()

#import matplotlib.pyplot as plt   
# plt.plot(ECG_data)
# plt.plot(peaks2, ECG_data[peaks2], "x")
# plt.xlim(720, 2720) #Visualizing after 2s of initializing time
# plt.xlabel("Numer próbki [-]")
# plt.grid("on")
# plt.ylim(0,1) #If the signal was normalized the values will be in [0,1] range
# plt.ylabel("Amplituda [mV]")
# plt.show()