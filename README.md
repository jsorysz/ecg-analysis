**ECG Analysis**

_**Introduction**_
In this project the aim was to create an application to perform an analysis of ECG signal. To achieve this goal, python programming language for creating algorithms was used and to create GUI the PyQT5 was chosen as it connects the pros of QT with python.

**_Technologies_**
Program was tested in Python 3.9.2.

_**Libraries**_
numpy 1.21
scipy 1.7.1
wfdb 3.4.1
Matplotlib 3.4.3
PyWavelets 1.1.1
Pandas 1.3.4
PyEMD 0.2.13
EMD0signal 1.1.0
hrvanalysis 1.0.4
noldss 0.4.1
future 0.16.0
astropy 3.0.4
pyHRV 0.4.0
scikit-learn 1.0.1
joblil 1.0.1

_**Launch**_

To launch this project one needs to have all the libraries, which were mentioned in the previous section with those specific versions. If different ones were to be used, there can be no guarantee that the application will start correctly. Fulfilling the requirements,one can go to the program folder and in a console open the main.py file, which will cause a start of the application.

_**Data**_

Required data is in the form of the ECG files in three extensions: *.hea *.atr and *.dat. and it should be located in one directory.

_Acknowledgement_

The project was created as a part of Dedicated Algorithms in Medical Diagnostics class, under the supervision of dr Tomasz Pięciak.


